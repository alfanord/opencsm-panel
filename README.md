#### OpenCSM Web-Panel

___

1.  Create sample configuration file `config.py`

```python
class Config(object):
    CSRF_ENABLED = True
    SECRET_KEY = 'very-fucking-random-string'  # change me
    RECAPTCHA_PUBLIC_KEY = 'google-recaptca-public-key' # change me
    RECAPTCHA_PRIVATE_KEY = 'google-recaptcha-private-key' # change me
    RECAPTCHA_DATA_ATTRS = {'callback': 'verifyCallback'}
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_DATABASE_URI = 'postgresql://dbuser:dbpass@dbhost:dbport/dbname' #change me
    OPENCSM_HOST = "ip:port" # change me
    OPENCSM_KEY = "very-fucking-random-string" #change me
    OPENCSM_CID = "130" # CID code to send
```

2.  Run docker

```shell
$ docker run -d -p 127.0.0.1:80:8000 \
             -e TZ=Europe/Moscow \
             -v $(pwd)/config.py:/usr/src/app/config.py \
             hub.an-direct.ru/python/opencsm-panel
```