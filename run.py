# -*- coding: utf-8 -*-
from opencsm import app, socketio, pbx, pgnotify
from threading import Thread


if __name__ == "__main__":

    if pgnotify:
        Thread(target=pgnotify.start).start()
    if pbx:
        Thread(target=pbx.run).start()

    socketio.run(app, host='0.0.0.0')
