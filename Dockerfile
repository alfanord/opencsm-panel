FROM python:3.6

WORKDIR /usr/src/app

COPY . .

RUN pip install --no-cache-dir -r requirements.txt && \
    apt-get update && apt-get install --no-install-recommends -y locales supervisor && \
    locale-gen ru_RU.UTF-8 ru_RU && \
    localedef -i ru_RU -c -f UTF-8 -A /usr/share/locale/locale.alias ru_RU.UTF-8 && \
    localedef -i ru_RU -c -f UTF-8 -A /usr/share/locale/locale.alias ru_RU && \
    pip install --upgrade https://github.com/eventlet/eventlet/archive/84cc764afdf83370ec236163e9ab05f0dad9c6e7.zip && \
    apt-get clean all

ENV TZ=Europe/Moscow \
    LANG=ru_RU.UTF-8 \
    LANGUAGE=ru_RU.UTF-8

CMD supervisord -c /usr/src/app/supervisord.conf


    