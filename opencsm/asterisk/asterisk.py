# -*- coding: utf-8 -*-
# noinspection SpellCheckingInspection
import ari
import uuid
from requests import HTTPError
from opencsm.models import Person, Object, Partition
from opencsm import app
from opencsm import ws
import logging


class DictPayload(object):
    def __init__(self, data):
        self.__dict__.update(data)


class PersonIdentify(object):
    def __init__(self, channel):
        self.chan = channel.get('channel')
        self.caller = self.chan.json.get('caller').get('number')
        self.run()

    def run(self):
        try:
            logging.info('Starting Person check for {}'.format(self.caller))
            playback_id = str(uuid.uuid4())
            # playback = self.chan.playWithId(playbackId=playback_id, media='sound:demo-congrats')
            # playback = self.chan.playWithId(playbackId=playback_id, media='sound:ans-ivr-gratz-8')
            self.chan.playWithId(playbackId=playback_id, media='sound:ans-ivr-gratz-8')
            self.chan.playWithId(playbackId=playback_id, media='sound:ans-ivr-stay-8')
            # playback.on_event('PlaybackFinished', self.playback_finished)
            # self.chan.playWithId(playbackId=playback_id, media='sound:ans-ivr-stay-8')

            with app.app_context():
                person = Person.query.filter_by(phone_mobile=self.caller).first()
                if person is not None:
                    full_name = person.full_name
                    prio = person.priority[0]
                    callerid_name = '{} ({})'.format(full_name, person.priority[1])
                    self.chan.setChannelVar(variable='CALLERID(name)', value=callerid_name)
                    self.chan.setChannelVar(variable='QUEUE_PRIO', value=prio)
                    logging.warning('New call from {}'.format(callerid_name))

        except HTTPError as e:
            logging.error(e)
        finally:
            self.chan.continueInDialplan()


class ObjectControl(object):
    def __init__(self, channel):
        self.person = None
        # self.object = None
        self.person_input = ''
        self.channel = channel.get('channel')
        self.caller = self.channel.json.get('caller').get('number')
        self.run()

    # def report_state(self):
    #     print(self.object.partition_states)
    #     with app.app_context():
    #         all_parts = self.object.partitions.all()
    #         print(all_parts)
    #         for part in all_parts:
    #             print(part.code_phonetic)

    def process_input(self):
        logging.debug(self.person_input)
        print(self.person)
        if len(self.person_input) == 4:
            for person_object in self.person.objects:
                if person_object.panel_code == self.person_input:
                    print(person_object.partition_states)
                    # self.object = person_object
                    # self.report_state()
                else:
                    self.person_input = ''
                    playback_id = str(uuid.uuid4())
                    self.channel.playWithId(playbackId=playback_id, media='sound:check-number-dial-again')
            # print(self.person.objects)
        else:
            self.person_input = ''
            playback_id = str(uuid.uuid4())
            self.channel.playWithId(playbackId=playback_id, media='sound:check-number-dial-again')

    def on_dtmf(self, channel, event):
        digit = event['digit']
        if digit == '#':
            self.process_input()
            # channel.continueInDialplan()
        elif digit == '*':
            self.person_input = ''
            # channel.play(media='sound:asterisk-friend')
        else:
            self.person_input += str(digit)
            # channel.play(media='sound:digits/{}'.format(digit))

    def run(self):
        try:
            logging.info('Starting Person check for {}'.format(self.caller))
            playback_id = str(uuid.uuid4())
            # playback = self.chan.playWithId(playbackId=playback_id, media='sound:demo-congrats')
            # playback = self.chan.playWithId(playbackId=playback_id, media='sound:check-number-dial-again')
            # self.channel.playWithId(playbackId=playback_id, media='sound:hello')
            self.channel.playWithId(playbackId=playback_id, media='sound:panel')
            # playback.on_event('PlaybackFinished', self.playback_finished)
            # self.chan.playWithId(playbackId=playback_id, media='sound:ans-ivr-stay-8')

            with app.app_context():
                person = Person.query.filter_by(phone_mobile=self.caller).first()
                if person is not None:
                    self.person = person
                    full_name = person.full_name
                    prio = person.priority[0]
                    callerid_name = '{} ({})'.format(full_name, person.priority[1])
                    self.channel.setChannelVar(variable='CALLERID(name)', value=callerid_name)
                    self.channel.setChannelVar(variable='QUEUE_PRIO', value=prio)
                    data = {'person': callerid_name,
                            'phone': self.caller,
                            'prio': person.priority[1]}
                    ws.bg_emit('pbx', 'pbx_event', data)
                    self.channel.on_event('ChannelDtmfReceived', self.on_dtmf)
                    logging.warning('New call from {}'.format(callerid_name))

        except HTTPError as e:
            logging.error(e)
            self.channel.continueInDialplan()


class Asterisk(object):
    # noinspection PyBroadException
    apps = dict()

    def __init__(self):
        self.client = ari.connect('http://{}'.format(app.config['ARI_HOST']), app.config['ARI_USERNAME'], app.config['ARI_PASSWORD'])
        self.client.on_channel_event('StasisStart', self.stasis_start_cb)
        self.client.on_channel_event('StasisEnd', self.stasis_end_cb)

    def stasis_end_cb(self, channel, ev):
        # application = ev.get('application')
        channel_id = channel.json.get('id')
        if channel_id in self.apps:
            del self.apps[channel_id]
        logging.debug('Stasis ends {}: {}'.format(channel, ev))

    def stasis_start_cb(self, channel, ev):
        application = ev.get('application')
        statis_chan = channel.get('channel')
        channel_id = statis_chan.json.get('id')
        logging.debug('Stasis starts {}: {}'.format(channel_id, ev))
        if application == 'PersonIdentify':
            self.apps.update({channel_id: PersonIdentify(channel)})
        elif application == 'ObjectControl':
            self.apps.update({channel_id: ObjectControl(channel)})
        logging.debug(self.apps)

    @property
    def channels(self):
        return self.client.channels.list()

    # noinspection PyBroadException
    def run(self):
        try:
            self.client.run(apps=app.config['ARI_APPS'])
        except Exception as e:
            logging.error(e)
            pass
