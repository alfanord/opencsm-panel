jQuery.expr[':'].regex = function(elem, index, match) {
    var matchParams = match[3].split(','),
        validLabels = /^(data|css):/,
        attr = {
            method: matchParams[0].match(validLabels) ?
                matchParams[0].split(':')[0] : 'attr',
            property: matchParams.shift().replace(validLabels, '')
        },
        regexFlags = 'ig',
        regex = new RegExp(matchParams.join('').replace(/^\s+|\s+$/g, ''), regexFlags);
    return regex.test(jQuery(elem)[attr.method](attr.property));
}

function update_dash_objects() {

    $.ajax({
        url: '/_objects/1',
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            // $('#fa-top10-objects').replaceWith('<i class="fa fa-circle-o-notch fa-spin fa-lg fa-fw" aria-hidden="true" id="fa-top10-objects"></i>');
        },

        success: function(data) {
            console.log(data);
            // $('#list-top10-objects').empty();
            // $.each(data, function(key, value) {
            // $('#list-top10-objects').append('<a href="/object/' + value.id + '" class="list-group-item"><span class="badge">' +
            // value.troubles_count + '</span>' + value.id_str.substr(6) + ' (' + value.title + ')</a>');
            // });
        },
        complete: function() {
            // $('#fa-top10-objects').replaceWith('<i class="fa fa-bar-chart fa-lg fa-fw" aria-hidden="true" id="fa-top10-objects"></i>');

        }
    });
};


$(document).ready(function() {

    // $('.object-images').viewer({
    //     movable: true,
    //     // shown: function() {
    //     //     console.log($(this));
    //     //     $('#page-wrapper').on('click', function(e) {
    //     //         e.preventDefault();
    //     //         console.log('clicked');
    //     //     });

    //     //     // $('.object-images').viewer('hide');
    //     // },
    // });

    // update_dash_objects();

    $('.person-popover').popover({
        html: true,
        placement: 'bottom',
        trigger: 'hover'
    });


    console.log(lat);
    console.log(lon);


    var raster = new ol.layer.Tile({
        source: new ol.source.OSM()
    });


    var iconFeatures = [];

    var iconFeature = new ol.Feature({
        geometry: new ol.geom.Point(ol.proj.transform([lon, lat], 'EPSG:4326',
            'EPSG:3857')),
        name: 'Null Island',
        population: 4000,
        rainfall: 500
    });

    iconFeatures.push(iconFeature);

    var vectorSource = new ol.source.Vector({
        features: iconFeatures //add an array of features
    });

    var iconStyle = new ol.style.Style({
        image: new ol.style.Icon( /** @type {olx.style.IconOptions} */ ({
            anchor: [0.5, 46],
            anchorXUnits: 'fraction',
            anchorYUnits: 'pixels',
            opacity: 0.75,
            src: '/static/img/maps-and-flags.png'
        }))
    });

    var map = new ol.Map({
        target: 'object_map',
        controls: ol.control.defaults().extend([
            new ol.control.FullScreen()
        ]),
        layers: [raster,
            new ol.layer.Vector({
                source: vectorSource,
                style: iconStyle
            })
        ],
        view: new ol.View({
            center: ol.proj.fromLonLat([lon, lat]),
            zoom: 15
        })
    });

    $("#object-images").lightGallery({
        thumbnail: true
    });

    socket.on('connect', function() {
        socket.emit('join', { room: 'message' });
        socket.emit('join', { room: 'object' });
        socket.emit('join', { room: 'trouble' });
    });


    socket.on('db_event', function(payload) {
        // console.log(payload)
        var payload_table = payload.table;
        var action = payload.action;
        var data = payload.data;
        // var object_id = $('.dt-messages').attr('id');

        // console.log(object_id);

        if (payload_table == 'message' && data.object_id == object_id) {

            // var q = 'E';

            // if (data.qualifier == '1') {
            //     q = 'E';
            // } else {
            //     q = 'R'
            // }

            // var panel_code = data.object_code.substr(6, 4);

            // var contact_id = q + data.event_code + data.section_code + data.zone_code;
            // var channel_title = null;
            // var event_type_title = null;

            // if (data.channel != null) {
            //     channel_title = data.channel.title;
            // }

            // if (data.event_type_json != null) {
            //     event_type_title = data.event_type_json.title;
            // }


            // $('.dt-messages').DataTable().row.add({
            //     'timestamp': moment(data.received_at).locale('ru').format('MMM DD HH:mm:ss'),
            //     'channel_title': channel_title,
            //     'event_type_title': event_type_title,
            //     'event_text': data.text,
            //     'object_title': data.object.status_icon + ' ' + data.object.title,
            //     'object_address': data.object.address,
            //     'user_comment': data.usercomment,
            //     'panel_code': panel_code,
            //     'event_text': data.text,
            //     'event_code': data.event_code,
            //     'zone_code': data.zone_code,
            //     'partition_code': data.section_code,
            //     'object_id': data.object_id,
            //     'trouble_id': data.trouble_id,
            //     "event_type": data.event_type,
            // }).draw();
            // $(rowNode).addClass("info");
        } else if (payload_table == 'trouble') {
            // update_dashboard();
            // update_dash_objects();
        }


    });

    socket.on('pbx_event', function(data) {
        console.log(data);
        show_noty('info', data.person + ', prio:' + data.prio)
    });



    socket.on('my_response', function(msg) {
        console.log(msg);
    });




});