function make_flash(level, msg) {
    $('#alert_placeholder').html('<div class="alert alert-' + level + ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><span>' + msg + '</span></div>');
    window.setTimeout(function() { $(".alert").alert('close'); }, 2000);
}

var verifyCallback = function(response) {
    $('#login-form').submit();
    console.log('g-recaptcha-response: ' + response);
};

function set_h3_date(date) {

    $('#current-date').html(date.toDate().toLocaleString('ru', {
        year: 'numeric',
        month: 'long',
        day: 'numeric'
    }));
}

// $(window).on('resize', function() {
//     if (!window.recentResize) {
//         window.m.redraw();
//         window.recentResize = true;
//         setTimeout(function() { window.recentResize = false; }, 200);
//     }
// });


function draw_call_charts(params) {

    $('#bar-chart-957').empty();

    $.getJSON("/_daily_chart", function(data) {
        console.log(data);

        Morris.Bar({
            element: 'bar-chart-957',
            data: data,
            xkey: 'y',
            ykeys: ['a'],
            labels: ['975'],
            hideHover: 'auto',
            resize: true,
            stacked: true
        });
    });


}


function draw_call_table(params) {
    try {

        if ($.fn.DataTable.isDataTable(params.table)) {
            $(params.table).DataTable().destroy();
        }

        $(params.table).empty();

        var table = $(params.table).DataTable({
            responsive: {
                details: true
            },
            ajax: function(data, callback, settings) {

                $.ajax({
                    url: params.url,
                    type: 'GET',
                    data: { date: params.date },
                    beforeSend: function() {
                        $('.fa-calls-table').replaceWith('<i class="fa fa-circle-o-notch fa-spin fa-lg fa-fw fa-calls-table" aria-hidden="true"></i>');
                    },

                    success: function(data) {
                        callback(JSON.parse(data));
                        $(params.table.replace('#', '.') + '-counter').text(table.data().count()); //TODO: update after responsive tab show
                    },
                    complete: function() {
                        $('.fa-calls-table').replaceWith('<i class="fa fa-calendar-o fa-lg fa-fw fa-calls-table" aria-hidden="true"></i>');
                    }
                });
            },

            lengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "Все"]
            ],
            iDisplayLength: 10,
            ordering: false,

            columns: [
                { data: "timestamp", title: "Время", responsivePriority: 1 },
                { data: "kto", title: "Кто", responsivePriority: 2 },
                { data: "kuda", title: "Куда", responsivePriority: 4 },
                { data: "record", title: "Запись", responsivePriority: 3 }
            ],

            language: {
                lengthMenu: "_MENU_  записей на странице",
                info: "Страница _PAGE_ из _PAGES_",
                infoFiltered: "(отфильтровано из _MAX_ записей)",
                search: "Фильтр:",
                zeroRecords: "Ничего не найдено :(",
                infoEmpty: "0 из 0 записей",
                paginate: {
                    first: "Начало",
                    last: "Конец",
                    next: "След.",
                    previous: "Пред."
                }
            }

        });

        $(params.table + '_wrapper>div:nth-child(1)>div:nth-child(1)').addClass('hidden-xs');


    } catch (ex) {
        console.log(ex.message);
    }
}


$(function() {

    try {
        var dtp = $('#calls-dtp').datetimepicker({
            locale: 'ru',
            format: 'LL',
            showTodayButton: true,
            defaultDate: new Date(),
            icons: {
                today: 'fa fa-dot-circle-o fa-lg',
                date: 'fa fa-calendar-o',
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right'
            }
        });

        dtp.on('dp.change', function(e) {

            var selected_date = e.date;

            set_h3_date(selected_date);

            var call_tables = [{
                    table: '#dt-incoming',
                    url: '/_calls_incoming',
                    date: selected_date.format('YYYY-MM-DD')
                },
                {
                    table: '#dt-outgoing',
                    url: '/_calls_outgoing',
                    date: selected_date.format('YYYY-MM-DD')
                },
                {
                    table: '#dt-unanswered',
                    url: '/_calls_unanswered',
                    date: selected_date.format('YYYY-MM-DD')
                }
            ];

            for (var i = 0, len = call_tables.length; i < len; i++) {
                draw_call_table(call_tables[i]);
            }


        });

        set_h3_date(moment());

    } catch (ex) {
        console.log(ex.message);
    }


    var call_tables = [{
            table: '#dt-incoming',
            url: '/_calls_incoming',
            date: moment().format('YYYY-MM-DD')
        },
        {
            table: '#dt-outgoing',
            url: '/_calls_outgoing',
            date: moment().format('YYYY-MM-DD')
        },
        {
            table: '#dt-unanswered',
            url: '/_calls_unanswered',
            date: moment().format('YYYY-MM-DD')
        }
    ];

    for (var i = 0, len = call_tables.length; i < len; i++) {
        draw_call_table(call_tables[i]);
    }


    fakewaffle.responsiveTabs(['xs', 'sm']);


    $('a[data-toggle="tab"]').on("shown.bs.tab", function(e) {

        var tables = $.fn.dataTable.tables(true);

        $(tables).DataTable().columns.adjust().responsive.recalc();

        console.log($(tables).DataTable().data().count());

        // for (var i = 0, len = call_tables.length; i < len; i++) {
        //     // $(call_tables[i] + '-counter').text(call_tables[i].data().count());
        //     var oTable = $(call_tables[i]).dataTable();
        //     console.log(oTable.data().count());

        // }

    });


    socket.on('connect', function() {
        socket.emit('join', { room: 'pbx' });
    });


    socket.on('pbx_event', function(data) {
        console.log('EMIT EMIT EMIT');
        console.log(data);
        show_noty('info', JSON.stringify(data))
    });

    socket.on('my_response', function(data) {
        console.log(data);
    });



    // draw_call_charts(null);

});


$(document).on('click', '.btn-make-alarm', function(e) {
    e.preventDefault();

    var params = {
        "code": this.id,
    };

    $.ajax({
        url: "/_make_alarm",
        type: "post",
        contentType: "application/json",
        data: JSON.stringify(params),
        processData: false,
        global: false,
        error: function() {
            make_flash('danger', "Тревога не отправлена!");
        },
        success: function() {
            make_flash('success', "Тревога отправлена!");
        }

    });
});