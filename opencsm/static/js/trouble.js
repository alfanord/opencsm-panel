function add_to_timeline(data) {
    // console.log('data= ' + data);
    $('.timeline-troubles').append('<li class="timeline-item"><div class="timeline-badge ' + data.event_type_json.fill_color + '"><i class="fa fa-check"></i></div>' +
        '<div class="timeline-panel"><div class="timeline-heading"><h4 class="timeline-title"><i class="fa fa-clock-o"></i>&nbsp;' +
        moment(data.received_at).locale('ru').format('MMM DD, HH:mm:ss') + '</h4></div>' +
        '<div class="timeline-body" data-toggle="tooltip" data-placement="bottom" title="' + data.event_type_json.title + '"><p>' + data.text + '</p>' +
        '<p><small class="text-muted">' + data.usercomment + '</small></p>' + '</div></div></li>');

    $('[data-toggle="tooltip"]').tooltip({
        container: 'body'
    });
}

$(document).ready(function() {

    console.log(trouble_id, object_id);
    console.log(troubles_list);

    socket.on('connect', function() {
        socket.emit('join', { room: 'wialon_units' });
        socket.emit('join', { room: 'mobile_group' });
        socket.emit('join', { room: 'message' });
    });


    socket.on('db_event', function(payload) {

        var db_table = payload.table;
        var action = payload.action;
        var data = payload.data;

        if (db_table == 'message') {
            if (action == 'INSERT' && data.trouble_id == trouble_id) {
                add_to_timeline(data);
            }
        }

    });


    $('.person-popover').popover({
        html: true,
        placement: 'bottom',
        trigger: 'hover'
    });

    var iconFeatures = [];

    var vectorSource = new ol.source.Vector({
        features: iconFeatures //add an array of features
    });

    var iconStyle = new ol.style.Style({
        image: new ol.style.Icon( /** @type {olx.style.IconOptions} */ ({
            anchor: [0.5, 46],
            anchorXUnits: 'fraction',
            anchorYUnits: 'pixels',
            opacity: 0.75,
            src: '/static/img/maps-and-flags.png'
        }))
    });


    // var iconStyle2 = new ol.style.Style({
    //     image: new ol.style.Icon( /** @type {olx.style.IconOptions} */ ({
    //         // anchor: [0.5, 46],
    //         // anchorXUnits: 'fraction',
    //         // anchorYUnits: 'pixels',
    //         opacity: 0.75,
    //         src: '/static/img/route.png'
    //     }))
    // });


    var iconStyle2 = new ol.style.Style({
        image: new ol.style.Circle({
            radius: 6,
            stroke: new ol.style.Stroke({
                color: 'white',
                width: 2
            }),
            fill: new ol.style.Fill({
                color: 'red'
            })
        })
    });
    // console.log(route);




    var iconGeometry = new ol.geom.Point(ol.proj.transform([lon, lat], 'EPSG:4326', 'EPSG:3857'));
    var iconFeature = new ol.Feature({
        geometry: iconGeometry
    });
    // console.log(tr.object.lon);
    iconFeatures.push(iconFeature);

    var vectorLayer2 = new ol.layer.Vector({
        source: vectorSource,
        // source: new ol.source.Vector({
        // features: [feature]
        // }),
        style: iconStyle
    });

    vectorSource.addFeatures(iconFeatures);

    var raster = new ol.layer.Tile({
        source: new ol.source.OSM()
    });

    // console.log(feature);

    var map = new ol.Map({
        target: 'trouble_map',
        controls: ol.control.defaults().extend([
            new ol.control.FullScreen()
        ]),
        // layers: [raster,
        //     new ol.layer.Vector({
        //         source: vectorSource,
        //         style: iconStyle
        //     })
        layers: [raster, vectorLayer2],
        view: new ol.View({
            center: ol.proj.fromLonLat([lon, lat]),
            zoom: 12
        })
    });
    console.log(routes);
    routes.forEach(function(route) {
        console.log(route);

        if (route != 'None') {

            var features = [];

            var format = new ol.format.WKT();

            var feature = format.readFeature(route, {
                dataProjection: 'EPSG:4326',
                featureProjection: 'EPSG:3857'
            });


            var style = new ol.style.Style({
                stroke: new ol.style.Stroke({ color: "green", width: "4" })
            });

            feature.setStyle(style);

            start_point = feature.getGeometry().getFirstCoordinate();
            end_point = feature.getGeometry().getLastCoordinate();


            var startGeometry = new ol.geom.Point(start_point);
            var endGeometry = new ol.geom.Point(end_point);

            var startFeature = new ol.Feature({
                geometry: startGeometry
            });
            var endFeature = new ol.Feature({
                geometry: endGeometry
            });

            startFeature.setStyle(iconStyle2);
            endFeature.setStyle(iconStyle2);
            // console.log(tr.object.lon);
            features.push(startFeature);
            features.push(endFeature);

            features.push(feature);

            // iconFeatures.push(feature);


            var vectorLayer = new ol.layer.Vector({
                // source: vectorSource,
                source: new ol.source.Vector({
                    features: features
                }),
                // style: iconStyle
            });

            map.addLayer(vectorLayer);
        }
    }, this);

    fakewaffle.responsiveTabs(['xs', 'sm']);

    $("#object-images").lightGallery({
        thumbnail: true
    });

});