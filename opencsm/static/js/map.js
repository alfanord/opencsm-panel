jQuery.expr[':'].regex = function(elem, index, match) {
    var matchParams = match[3].split(','),
        validLabels = /^(data|css):/,
        attr = {
            method: matchParams[0].match(validLabels) ?
                matchParams[0].split(':')[0] : 'attr',
            property: matchParams.shift().replace(validLabels, '')
        },
        regexFlags = 'ig',
        regex = new RegExp(matchParams.join('').replace(/^\s+|\s+$/g, ''), regexFlags);
    return regex.test(jQuery(elem)[attr.method](attr.property));
}



var raster = new ol.layer.Tile({
    source: new ol.source.OSM()
});

var mg_source = new ol.source.Vector({
    features: []
});

var mg_vector = new ol.layer.Vector({
    source: mg_source
});


var map = new ol.Map({
    layers: [raster],
    controls: ol.control.defaults().extend([
        new ol.control.FullScreen()
    ]),
    target: 'global_map',
    view: new ol.View({
        center: ol.proj.fromLonLat([30.3601, 59.9476]),
        // projection: projection,
        zoom: 10
    })
});

// Vienna label
// var mg_list = new ol.Overlay({
//     position: [30, 30],
//     element: document.getElementById('mobile_group_list')
// });
// map.addOverlay(mg_list);


var popup = new ol.Overlay({
    element: document.getElementById('mobile_group_popup')
});

map.addOverlay(popup);


var mobile_groups = [];



var displayFeatureInfo = function(pixel) {



    var features = [];

    map.forEachFeatureAtPixel(pixel, function(feature) {
        features.push(feature);
    });

    if (features.length > 0) {
        var mg = mobile_groups.find(function(element, index, array) { return (features[0] == element.feature) });

        if (mg) { mg.popover(); }
    }


};

// map.on('pointermove', function(evt) {
//     if (evt.dragging) {
//         return;
//     }
//     var pixel = map.getEventPixel(evt.originalEvent);
//     displayFeatureInfo(pixel);
// });

map.on('click', function(evt) {
    $(popup.getElement()).popover('destroy');
    displayFeatureInfo(evt.pixel);
});

$(map).dblclick(function() {
    $(popup.getElement()).popover('destroy');
});

map.addLayer(mg_vector);
map.addControl(new ol.control.ZoomSlider());

Number.prototype.toRad = function() { return this * Math.PI / 180; }



$(document).ready(function() {

    $("#global_map").css("height", $(window).height() - 140);

    map.updateSize();


    var mg_table = $('#dt_map_mg').DataTable({
        responsive: true,
        colReorder: true,
        pagingType: "simple",

        columns: [
            { data: "title", title: "Все МЭ", orderable: true, responsivePriority: 10001 },
            // { data: "name", title: "name", orderable: true, responsivePriority: 10010 },
            {
                data: "id",
                title: '<div class="checkbox checkbox-primary"><input type="checkbox" id="mg_toggle" checked><label></label></div>',
                sortable: false,
                orderable: false,
                responsivePriority: 10000,
                render: function(data, type, full, meta) {
                    return '<div class="checkbox"><input class="mg_checkbox" id="mg_' + data + '" type="checkbox" checked><label></label></div>';
                }
            },
            // {
            //     data: "status",
            //     title: "S",
            //     orderable: true,
            //     render: function(data, type, full, meta) {
            //         return '<img height="24" src="' + data + '">'
            //     }
            // }
        ],
        // dom: '<"panel panel-default"<"panel-heading"f>t<"panel-footer"p>>',
        dom: '<"col-xs-6 col-mg"f><"col-xs-6 col-mg"p>t',
        iDisplayLength: 20,
        order: [0, 'asc'],

        language: {
            lengthMenu: "_MENU_  записей на странице",
            info: "Страница _PAGE_ из _PAGES_",
            infoFiltered: "(отфильтровано из _MAX_ записей)",
            search: '',
            zeroRecords: '<i class="fa fa-spinner fa-spin fa-lg"></i> loading...',
            infoEmpty: "0 из 0 записей",
            paginate: {
                first: "Начало",
                last: "Конец",
                next: '<i class="fa fa-chevron-circle-right fa-lg"></i>',
                previous: '<i class="fa fa-chevron-circle-left fa-lg"></i>'
            }
        },
        drawCallback: function(settings) {
            this.DataTable().columns.adjust().responsive.recalc();
        }
    });

    $('#dt_map_mg tbody').on('click', 'td', function() {
        $(popup.getElement()).popover('destroy');
        if ($(this).index() != 1) { // provide index of your column in which you prevent row click here is column of 4 index
            var id = mg_table.row(this).data().id;
            var mg = mobile_groups.find(function(element, index, array) { return (id == element.id) });

            if (mg) {
                map.getView().animate({
                    center: ol.proj.fromLonLat([mg.wialon_unit.lon, mg.wialon_unit.lat]),
                    duration: 500
                });
                mg.popover();
            }


        }
        // console.log(mg_table.cell(this).data());


    });


    $.ajax({
        url: '/_mobile_groups',
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            // $('#fa-top10-objects').replaceWith('<i class="fa fa-circle-o-notch fa-spin fa-lg fa-fw" aria-hidden="true" id="fa-top10-objects"></i>');
        },

        success: function(data) {
            // console.log(data);
            for (i = 0; i < data.length; i++) {
                var mg = new MobileGroup(data[i]);
                mg.setup_feature();
                mg.setup_icon();
                mg.show_on_map(mg_source);
                mg.add_to_table();
                mobile_groups.push(mg);
            }
            // $('#list-top10-objects').empty();
            // $.each(data, function(key, value) {
            // $('#list-top10-objects').append('<a href="/object/' + value.id + '" class="list-group-item"><span class="badge">' +
            // value.troubles_count + '</span>' + value.id_str.substr(6) + ' (' + value.title + ')</a>');
            // });
        },
        complete: function() {
            $('#dt_map_mg').on('click', '.mg_checkbox', function(ev) {
                var id = parseInt($(ev.target).attr('id').split('_')[1]);
                var mg = mobile_groups.find(function(element, index, array) { return (id == element.id) });
                if ($(ev.target).prop('checked')) { mg.show_on_map(mg_source); } else { mg.remove_from_map(mg_source); }
            });

            $('#dt_map_mg').on('click', '#mg_toggle', function(ev) {
                var is_checked = $(ev.target).prop('checked');
                $('.mg_checkbox').prop('checked', is_checked);

                if (is_checked) {
                    for (i = 0; i < mobile_groups.length; i++) {
                        mobile_groups[i].show_on_map(mg_source);
                    }
                } else {
                    mg_source.clear();
                }

                // var id = parseInt($(ev.target).attr('id').split('_')[1]);
                // var mg = mobile_groups.find(function(element, index, array) { return (id == element.id) });
                // if ($(ev.target).prop('checked')) { mg.show_on_map(mg_source); } else { mg.remove_from_map(mg_source); }
            });

        }
    });


    // console.log(troubles_list);

    fakewaffle.responsiveTabs(['xs', 'sm']);

    $('a[data-toggle="tab"]').on("shown.bs.tab", function(e) {

        var tables = $.fn.dataTable.tables(true);

        $(tables).DataTable().columns.adjust().responsive.recalc();

        console.log($(tables).DataTable().data().count());

        // for (var i = 0, len = call_tables.length; i < len; i++) {
        //     // $(call_tables[i] + '-counter').text(call_tables[i].data().count());
        //     var oTable = $(call_tables[i]).dataTable();
        //     console.log(oTable.data().count());

        // }

    });
    // console.log(mobile_groups);

    socket.on('connect', function() {
        socket.emit('join', { room: 'mobile_group' });
        socket.emit('join', { room: 'wialon_units' });
    });

    socket.on('db_event', function(payload) {
        console.log(payload)
        var db_table = payload.table;
        var action = payload.action;
        var data = payload.data;

        if (db_table == 'wialon_units' && action == 'UPDATE') {
            var mg = mobile_groups.find(function(element, index, array) { return (data.id == element.wialon_id) });
            if (mg != null) {
                mg.wialon_unit = data;
                mg.feature.getGeometry().setCoordinates(ol.proj.fromLonLat([mg.wialon_unit.lon, mg.wialon_unit.lat]));
                mg.setup_icon();
                // m.show_on_map(mg_source);
            }
        } else if (db_table == 'mobile_group' && action == 'UPDATE') {
            var mm = mobile_groups.find(function(element, index, array) { return (data.id == element.id) });
            if (mg != null) {
                mg.status = data.status;
                // m.feature.getGeometry().setCoordinates(ol.proj.fromLonLat([m.wialon_unit.lon, m.wialon_unit.lat]));
                mg.setup_icon();
            }
        }
    });


    socket.on('my_response', function(msg) {
        console.log(msg);
    });

});


// var raster1 = new ol.layer.Tile({
//     source: new ol.source.XYZ({
//         url: 'http://vec0{1-4}.maps.yandex.net/tiles?l=map&x={x}&y={y}&z={z}',
//         projection: 'EPSG:3395',
//         tileGrid: ol.tilegrid.createXYZ({
//             extent: [-20037508.342789244, -20037508.342789244, 20037508.342789244, 20037508.342789244]
//         })
//     })
// });


// var vector = new ol.layer.Vector({
//     source: new ol.source.Vector({
//         url: 'https://openlayers.org/en/v3.19.1/examples/data/kml/2012-02-10.kml',
//         // url: '/_get_kml',
//         format: new ol.format.KML()
//     })
// });



// var lon = parseFloat($("#longitude").html());
// var lat = parseFloat($("#latitude").html());

// console.log(lat);
// console.log(lon);


// var iconFeatures = [];

// var iconFeature = new ol.Feature({
//     geometry: new ol.geom.Point(ol.proj.transform([lon, lat], 'EPSG:4326',
//         'EPSG:3857')),
//     name: 'Null Island',
//     population: 4000,
//     rainfall: 500
// });

// iconFeatures.push(iconFeature);

// var vectorSource = new ol.source.Vector({
//     features: iconFeatures //add an array of features
// });

// var iconStyle = new ol.style.Style({
//     image: new ol.style.Icon( /** @type {olx.style.IconOptions} */ ({
//         anchor: [0.5, 46],
//         anchorXUnits: 'fraction',
//         anchorYUnits: 'pixels',
//         opacity: 0.75,
//         src: '/static/img/maps-and-flags.png'
//     }))
// });

// var map = new ol.Map({
//     target: 'map',
//     controls: ol.control.defaults().extend([
//         new ol.control.FullScreen()
//     ]),
//     layers: [
//         new ol.layer.Tile({
//             source: new ol.source.OSM()
//         }),
//         new ol.layer.Vector({
//             source: vectorSource,
//             style: iconStyle
//         })
//     ],
//     view: new ol.View({
//         center: ol.proj.fromLonLat([lon, lat]),
//         zoom: 15
//     })
// });