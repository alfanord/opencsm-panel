$(document).ready(function() {
    try {
        var table = $('#dt_objects').DataTable({

            "columnDefs": [{
                targets: [2, 3, 7, 8, 9],
                responsivePriority: 10001
            }],

            "drawCallback": function(settings) {
                this.DataTable().columns.adjust().responsive.recalc();
            }


        });

        $('[data-toggle="popover"]').popover();


    } catch (ex) {
        console.log(ex.message);
    }

});