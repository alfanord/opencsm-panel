var event_types_map = {
    '1': 'bg_Red',
    '2': 'bg_Red',
    '3': 'bg_Magenta',
    '4': 'bg_Green',
    '5': 'bg_CornflowerBlue',
    '6': 'bg_NavajoWhite',
    '7': 'bg_Coral',
    '8': 'bg_Coral',
    '9': 'bg_None',
    '10': 'bg_Coral',
    '11': 'bg_Yellow',
    '12': 'bg_CornflowerBlue',
    '13': 'bg_Green',
    '14': 'bg_None',
    '15': 'bg_None',
    '16': 'bg_None',
}

var point_color = {
    '1': 'Red',
    '2': 'Red',
    '3': 'Magenta',
    '4': 'Green',
    '5': 'CornflowerBlue',
    '6': 'NavajoWhite',
    '7': 'Coral',
    '8': 'Coral',
    '9': 'Grey',
    '10': 'Coral',
    '11': 'Yellow',
    '12': 'CornflowerBlue',
    '13': 'Green',
    '14': 'Grey',
    '15': 'Grey',
    '16': 'Grey',
}


var rooms = [];

var socket = io.connect('https://int.an-direct.ru/ocsm');

socket.on('connect', function() {
    console.log("WS Connected");
    $('#ws-status-icon').css({ "color": "black" });

    if ($('#auto_play_toggle').prop('checked') == true) {
        socket.emit('join', { room: 'message' });
    } else if ($('#auto_play_toggle').prop('checked') == false) {
        socket.emit('leave', { room: 'message' });
    }
});

socket.on('disconnect', function() {
    console.log("WS Disconnected");
    rooms = []
    $('#ws-status-icon').css({ "color": "indianred" });
});

socket.on('error', function() {
    console.log("WS Error");
    $('#ws-status-icon').css({ "color": "indianred" });
});

socket.on('my_response', function(data) {
    rooms = data.rooms;
    console.log(rooms);
});

function session_save(params) {
    $.ajax({
        url: "/_session",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(params)
    });
}

function MobileGroup(data) {
    this.id = data.id;
    this.wialon_id = data.wialon_id;
    this.status = data.status;
    this.phone_mobile = data.phone_mobile;
    this.title = data.title;
    this.wialon_unit = data.wialon_unit;
}

MobileGroup.prototype.setup_feature = function() {
    this.feature = new ol.Feature({
        geometry: new ol.geom.Point(ol.proj.transform([this.wialon_unit.lon, this.wialon_unit.lat], 'EPSG:4326', 'EPSG:3857'))
    });
};

MobileGroup.prototype.status_icon = function() {
    if (this.status == 1) {
        return '/static/img/L_21.png';
    } else if (this.status == 2) {
        return '/static/img/L_23.png';
    } else if (this.status == 3) {
        return '/static/img/L_19.png';
    } else {
        return '/static/img/L_25.png';
    }
}

MobileGroup.prototype.setup_icon = function() {
    var styles = [];

    styles.push(new ol.style.Style({
        image: new ol.style.Icon( /** @type {olx.style.IconOptions} */ ({
            anchor: [0.5, 0.5],
            // anchorXUnits: 'fraction',
            // anchorYUnits: 'pixels',
            opacity: 0.75,
            src: this.status_icon()
        }))
    }));

    if (this.wialon_unit.course > 0 && this.wialon_unit.speed > 0) {
        // console.log(this.title + ' speed=' + this.wialon_unit.speed + ' course=' + this.wialon_unit.course);

        styles.push(new ol.style.Style({
            // geometry: new ol.geom.Point(this.feature.getGeometry()),
            image: new ol.style.Icon({
                src: '/static/img/arrow.png',
                anchor: [0.5, 2],
                rotateWithView: true,
                rotation: this.wialon_unit.course.toRad()
            })
        }));

    }

    this.feature.setStyle(styles);
};



MobileGroup.prototype.add_to_table = function() {

    var rowNode = $('#dt_map_mg').DataTable().row.add({
        'title': '<img height="24" src="' + this.status_icon() + '">&nbsp;' + this.title,
        'id': this.id,
    }).draw().node();

    // $(rowNode).animate({ color: 'black' });
    // $(rowNode).attr('id', 'row_' + this.id);
};

MobileGroup.prototype.show_on_map = function(map_source) {
    map_source.addFeature(this.feature);
}

MobileGroup.prototype.remove_from_map = function(map_source) {
    map_source.removeFeature(this.feature);
}

MobileGroup.prototype.popover_content = function() {
    var lm = moment(this.wialon_unit.last_msg).locale('ru').format('MMM DD HH:mm:ss');
    return '<div>last_msg: <code>' + lm + '</code><br/>' +
        'speed: <code>' + this.wialon_unit.speed + '</code><br/>' +
        'course: <code>' + this.wialon_unit.course + '</code><br/>' +
        'name: <code>' + this.wialon_unit.nm + '</code><br/>' +
        'IMEI: <code>' + this.wialon_unit.uid + '</code><br/>' +
        'id: <code>' + this.wialon_unit.id + '</code></div>';
}

MobileGroup.prototype.popover = function() {
    // console.log('pop');
    var element = popup.getElement();

    popup.setPosition(this.feature.getGeometry().getCoordinates());

    $(element).popover('destroy');
    $(element).popover({
        'placement': 'top',
        'animation': false,
        'html': true,
        'title': this.title,
        'content': this.popover_content()
    });
    $(element).popover('show');
}

function Trouble(params) {
    this.id = params.id;
    this.opened = params.opened;
    this.status = params.status;
    this.opened_at = moment(params.opened_at);
    this.closed_at = null;
    this.start_work_time = null;
    this.event_type = params.event_type;
    this.object = params.object;
}

Trouble.prototype.make_feature = function() {
    this.feature = new ol.Feature({
        geometry: new ol.geom.Point(ol.proj.transform([this.object.lon, this.object.lat], 'EPSG:4326', 'EPSG:3857'))
    });
};

Trouble.prototype.style_feature = function() {
    var styles = [];

    styles.push(new ol.style.Style({
        image: new ol.style.Icon( /** @type {olx.style.IconOptions} */ ({
            anchor: [0.5, 1.3],
            anchorXUnits: 'fraction',
            anchorYUnits: 'fraction',
            opacity: 0.85,
            color: point_color[this.event_type.id],
            crossOrigin: 'anonymous',
            src: '/static/img/placeholder2.png'

        })),
        text: new ol.style.Text({
            text: this.object.panel_code,
            scale: 1.3,
            fill: new ol.style.Fill({
                color: '#000000'
            }),
            stroke: new ol.style.Stroke({
                color: '#FFFF99',
                width: 3.5
            })
        })
    }));



    this.feature.setStyle(styles);
};

Trouble.prototype.show_on_map = function(map_source) {
    map_source.addFeature(this.feature);
}

Trouble.prototype.remove_from_map = function(map_source) {
    map_source.removeFeature(this.feature);
}

Trouble.prototype.add_to_menu = function() {
    $('#troubles_menu').prepend('<li id="trouble_' + this.id + '"><a href="/troubles/' + this.id + '"> \
                <div> \
                <strong><i class="fa fa-home"></i> ' + this.object.panel_code + '</strong> \
                <span class="pull-right text-muted"> \
                <em><i class="fa fa-clock-o"></i> ' + this.opened_at.locale('ru').format('MMM DD HH:mm:ss') + '</em> \
                </span> \
                </div><div class="' + this.event_type.text_color + '">' + this.event_type.title + '</div> \
                <div>' + this.object.title + '</div> \
                </a></li>');
}

Trouble.prototype.remove_from_menu = function() {
    $('#trouble_' + this.id).remove();
}

function SafeObject(params) {
    this.id = params.id;
}

var object_id = $('#messages_button').attr('data-object');
var trouble_id = $('#messages_button').attr('data-trouble');

var troubles_list = [];

var troubles_source = new ol.source.Vector({
    features: []
});

$(document).ready(function() {

    $.ajax({
        url: '/_troubles',
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            // $('#fa-list-troubles').replaceWith('<i class="fa fa-circle-o-notch fa-spin fa-lg fa-fw" aria-hidden="true" id="fa-list-troubles"></i>');
        },

        success: function(data) {
            // vectorSource.clear();
            // iconFeatures = [];
            for (i = 0; i < data.length; i++) {
                //     // console.log(data.troubles_list[i]);
                var tr = new Trouble(data[i]);
                tr.make_feature();
                tr.style_feature();
                tr.add_to_menu();
                tr.show_on_map(troubles_source);
                troubles_list.push(tr);
                //     // var iconGeometry = new ol.geom.Point(ol.proj.transform([tr.object.lon, tr.object.lat], 'EPSG:4326', 'EPSG:3857'));
                //     // var iconFeature = new ol.Feature({
                //     // geometry: iconGeometry
                //     // });
                //     // console.log(tr.object.lon);
                //     // iconFeatures.push(iconFeature);

                //     //Do something
            }
            console.log(troubles_list);

            var countup_options = {
                useEasing: true,
                useGrouping: true,
                separator: ',',
                decimal: '.'
            };

            var counter = new CountUp('total_troubles', 0, troubles_list.length, 0, 1, countup_options);
            counter.start();
            // vectorSource.addFeatures(iconFeatures);


        },
        complete: function() {

            // $('#fa-list-troubles').replaceWith('<i class="fa fa-exclamation-triangle fa-lg fa-fw" aria-hidden="true" id="fa-list-troubles"></i>');
        }
    });

    socket.on('connect', function() {
        socket.emit('join', { room: 'trouble' });
    });

    socket.on('db_event', function(payload) {
        // console.log(payload)
        var payload_table = payload.table;
        var action = payload.action;
        var data = payload.data;


        if (payload_table == 'message' && action == 'INSERT' && object_id == '') {

            if (action == 'INSERT') {
                if (object_id == '' || data.object_id == object_id) {
                    var q = 'E';

                    if (data.qualifier == '1') {
                        q = 'E';
                    } else {
                        q = 'R'
                    }

                    var panel_code = data.object_code.substr(6, 4);

                    var contact_id = q + data.event_code + data.section_code + data.zone_code;
                    var channel_title = null;
                    var event_type_title = null;

                    if (data.channel != null) {
                        channel_title = data.channel.title;
                    }

                    if (data.event_type_json != null) {
                        event_type_title = data.event_type_json.title;
                    }


                    $('.dt-messages').DataTable().row.add({
                        'timestamp': moment(data.received_at).locale('ru').format('MMM DD HH:mm:ss'),
                        'channel_title': channel_title,
                        'event_type_title': event_type_title,
                        'event_text': data.text,
                        'object_title': data.object.status_icon + ' ' + data.object.title,
                        'object_address': data.object.address,
                        'user_comment': data.usercomment,
                        'panel_code': panel_code,
                        'event_text': data.text,
                        'event_code': data.event_code,
                        'zone_code': data.zone_code,
                        'partition_code': data.section_code,
                        'object_id': data.object_id,
                        'trouble_id': data.trouble_id,
                        "event_type": data.event_type,
                    }).draw();
                }
            }


        } else if (payload_table == 'trouble') {
            if (action == 'INSERT') {
                show_noty('info', '<h4>' + data.event_type_json.title + '</h4>' + data.object.title, 3000);
                data.event_type_id = data.event_type;
                data.event_type = data.event_type_json;
                console.log(data);
                var tr = new Trouble(data);
                troubles_list.push(tr);

                tr.make_feature();
                tr.style_feature();
                tr.add_to_menu();
                tr.show_on_map(troubles_source);

                var countup_options = {
                    useEasing: true,
                    useGrouping: true,
                    separator: ',',
                    decimal: '.'
                };

                var counter = new CountUp('total_troubles', 0, troubles_list.length, 0, 1, countup_options);
                counter.start();
            } else if (action == 'UPDATE' && data.opened == 0) {
                var tr = troubles_list.find(function(element, index, array) { return (data.id == element.id) });

                if (tr != null) {
                    tr_index = troubles_list.indexOf(tr);
                    tr.remove_from_menu();
                    tr.remove_from_map(troubles_source);
                    troubles_list.splice(tr_index, 1);

                    var countup_options = {
                        useEasing: true,
                        useGrouping: true,
                        separator: ',',
                        decimal: '.'
                    };

                    var counter = new CountUp('total_troubles', 0, troubles_list.length, 0, 1, countup_options);
                    counter.start();

                }
                console.log(troubles_list);
            }
        }
    });


    $('#messages_button').on('click', function(e) {



        var dialog = bootbox.dialog({
            title: function() {
                var div = document.createElement('div');
                div.className = "col-lg-9";

                var messages_title = document.createElement('span');
                messages_title.id = "messages_title";
                messages_title.innerHTML = ' Сообщения';

                var icon = document.createElement('i');
                icon.className = "fa fa-history";
                icon.id = "icon_messages"

                div.appendChild(icon);
                div.appendChild(messages_title);
                return div;
            },
            message: '<div class="table-responsive"><table class="table table-condensed table-bordered dt-responsive dt-messages nowrap" cellspacing="0" width="100%" id="' + object_id + '"></div>',
            className: "modal_messages",
            onEscape: true,
            backdrop: true
        });


        dialog.init(function() {
            var moment_end = moment();
            var moment_start = moment();

            if (object_id != '') {
                moment_start.add(-48, 'hours');
            } else {
                moment_start.add(-6, 'hours');
            }

            var date_end = moment_end.toDate();
            var date_start = moment_start.toDate();

            var messages_table = $('.dt-messages').DataTable({
                responsive: true,
                colReorder: true,
                ajax: function(data, callback, settings) {

                    $.ajax({
                        url: '/_messages',
                        type: 'GET',
                        data: { "date_start": moment_start.format(), "date_end": moment_end.format(), "object_id": object_id, "trouble_id": trouble_id },
                        beforeSend: function() {
                            $('i#icon_messages').addClass('fa-spin');
                            // $('#messages_title').text(' Обновление...');
                        },

                        success: function(data) {
                            // dialog.find('.bootbox-body').html('I was loaded after the dialog was shown!');

                            callback(JSON.parse(data));
                            // $(params.table.replace('#', '.') + '-counter').text(table.data().count()); //TODO: update after responsive tab show
                        },
                        complete: function() {
                            // $("#dtp_end").data("DateTimePicker").date(moment_end);
                            $('i#icon_messages').removeClass('fa-spin');
                            // $('#messages_title').text(' Сообщения');
                        }
                    });
                },

                order: [
                    [0, "desc"]
                ],

                lengthMenu: [
                    [10, 20, 50, 100, -1],
                    [10, 20, 50, 100, "Все"]
                ],

                drawCallback: function() {
                    // $('.dataTables_paginate > .pagination').addClass('pagination-sm');
                    $('div.dataTables_filter > label > input').removeClass('input-sm');
                    $('div.dataTables_length > label > select').removeClass('input-sm');
                    this.DataTable().columns.adjust().responsive.recalc();
                },

                iDisplayLength: 20,

                columns: [
                    { data: "timestamp", title: "Время", orderable: true, responsivePriority: 999 },
                    { data: "channel_title", title: "Канал", orderable: true, responsivePriority: 10005 },
                    { data: "event_type_title", title: "Событие", orderable: true, responsivePriority: 9999 },
                    {
                        data: "panel_code",
                        title: "Номер",
                        orderable: true,
                        class: "text-center",
                        responsivePriority: 10000,
                        render: function(data, type, full, meta) {
                            return '<a href="/objects/' + data.replace(/^[0]{1,3}/g, "") + '" target=_blank>' + data + '</a>'
                        }
                    },
                    { data: "object_title", title: "Объект", orderable: false, responsivePriority: 10000 },
                    { data: "event_text", title: "Сообщение", orderable: false, responsivePriority: 10001 },
                    { data: "user_comment", title: "Комментарий", orderable: false, responsivePriority: 10010 },
                    { data: "object_address", title: "Адрес", orderable: false, responsivePriority: 10010 },
                    { data: "event_code", title: "Кд", class: "text-center", orderable: true, responsivePriority: 10003 },
                    { data: "partition_code", title: "Р", class: "text-center", orderable: true, responsivePriority: 10003 },
                    { data: "zone_code", title: "Зн", class: "text-center", orderable: true, responsivePriority: 10003 },
                    { data: "object_id", orderable: false, visible: false, searchable: false },
                    {
                        data: "trouble_id",
                        title: "Инцидент",
                        orderable: true,
                        visible: true,
                        class: "text-center",
                        searchable: true,
                        responsivePriority: 10002,
                        render: function(data, type, full, meta) {
                            if (data != null) {
                                return '<a href="/troubles/' + data + '" target=_blank>' + data + '</a>';
                            } else {
                                return ''
                            }
                        }
                    },
                    { data: "event_type", orderable: false, visible: false, searchable: false }
                ],
                // pageLength: params.pageLength,

                dom: '<"row"<"col-xs-6 col-sm-3"l<"#auto_play.pull-right">><"col-xs-12 col-sm-3"f><"col-xs-12 col-sm-3"<"#dtp_start.input-group take-all-space">><"col-xs-12 col-sm-3"<"#dtp_end.input-group take-all-space">>>tp',

                createdRow: function(row, data, index) {
                    // if (data[5].replace(/[\$,]/g, '') * 1 > 150000) {
                    $('td', row).eq(2).addClass(event_types_map[data.event_type]);
                    // }
                    // $(this).columns.adjust().responsive.recalc();
                    // $('td', row).eq(2).text($('td', row).eq(1).text().substring(0, 48) + '...');
                    // $(row).addClass(event_types_map[data.event_type]);

                },

                language: {
                    lengthMenu: '_MENU_ <i class="fa fa-sort-amount-asc fa-fw" aria-hidden="true"></i>',
                    info: "Страница _PAGE_ из _PAGES_",
                    infoFiltered: "(отфильтровано из _MAX_ записей)",
                    search: '<i class="fa fa-search fa-fw"></i>',
                    zeroRecords: "Ничего не найдено :(",
                    infoEmpty: "0 из 0 записей",
                    paginate: {
                        first: "Начало",
                        last: "Конец",
                        next: "След.",
                        previous: "Пред."
                    }
                }

            });

            if (trouble_id != '') {
                messages_table.order(['0', "asc"]);
            }

            $('#dtp_start').html('<span class="input-group-addon">От:</span><input type="text" class="form-control"><span class="input-group-addon"><i class="fa fa-calendar-o fa-fw"></i></span></div>');
            $('#dtp_end').html('<span class="input-group-addon">До:</span><input type="text" class="form-control"><span class="input-group-addon"><i class="fa fa-calendar-o fa-fw"></i></span></div>');

            var dtp_end = $('#dtp_end').datetimepicker({
                locale: 'ru',
                format: 'MMM DD HH:mm:ss',
                showTodayButton: true,
                useCurrent: false,
                defaultDate: date_end,
                minDate: date_start,
                icons: {
                    today: 'fa fa-dot-circle-o fa-lg',
                    date: 'fa fa-calendar-o',
                    previous: 'fa fa-chevron-left',
                    next: 'fa fa-chevron-right'
                }
            });

            var dtp_start = $('#dtp_start').datetimepicker({
                locale: 'ru',
                format: 'MMM DD HH:mm:ss',
                showTodayButton: true,
                useCurrent: false,
                defaultDate: date_start,
                maxDate: date_end,
                icons: {
                    today: 'fa fa-dot-circle-o fa-lg',
                    date: 'fa fa-calendar-o',
                    previous: 'fa fa-chevron-left',
                    next: 'fa fa-chevron-right'
                }
            });

            $("#dtp_start").on("dp.change", function(e) {
                $('#dtp_end').data("DateTimePicker").minDate(e.date);

                if ($('#dtp_end').data("DateTimePicker").date() > $('#dtp_start').data("DateTimePicker").date()) {
                    moment_start = moment(e.date);
                    messages_table.ajax.reload();
                } else {
                    show_noty('error', 'wrong interval');
                }


            });
            $("#dtp_end").on("dp.change", function(e) {
                $('#dtp_start').data("DateTimePicker").maxDate(e.date);

                if ($('#dtp_end').data("DateTimePicker").date() > $('#dtp_start').data("DateTimePicker").date()) {
                    moment_end = moment(e.date);
                    messages_table.ajax.reload();
                } else {
                    show_noty('error', 'wrong interval');
                }

            });



            // $('#dt_messages_reload').on('click', function(e) {
            //     e.preventDefault();
            //     moment_end = moment();

            //     messages_table.ajax.reload();

            //     // $(this).button('reset');
            // });

            // $('#dt_messages_reload').mouseup(function() {
            //     $(this).blur();
            // });

            // $('#auto_play').css('display', 'inline-flex');

            console.log(rooms);

            if (rooms.indexOf('message') == -1) {
                $('#auto_play').append('<input class="pull-right" type="checkbox" id="auto_play_toggle">');
            } else {
                $('#auto_play').append('<input class="pull-right" type="checkbox" checked id="auto_play_toggle">');
                $("#dtp_end").data("DateTimePicker").disable();
                $("#dtp_start").data("DateTimePicker").disable();
            }

            $('#auto_play_toggle').bootstrapToggle();

            $('#auto_play_toggle').change(function() {

                if ($(this).prop('checked') == true) {
                    $("#dtp_end").data("DateTimePicker").date(moment());

                    if (rooms.indexOf('message') == -1) {
                        socket.emit('join', { room: 'message' });
                    };

                    $("#dtp_end").data("DateTimePicker").disable();
                    $("#dtp_start").data("DateTimePicker").disable();


                } else {

                    if (rooms.indexOf('message') != -1) {
                        socket.emit('leave', { room: 'message' });
                    };

                    $("#dtp_end").data("DateTimePicker").enable();
                    $("#dtp_start").data("DateTimePicker").enable();
                };



            })

        });

        // dialog.on('click', '#btn_close', function(e) {
        //     console.log(e);
        //     dialog.modal('hide');
        // });


        dialog.on('hide.bs.modal', function() {
            if (rooms.indexOf('message') == -1)
                socket.emit('join', { room: 'message' });
        });


        // dialog.css({
        //     'top': '50%',
        //     'margin-top': function() {
        //         return -(bootbox_promt.height() / 2);
        //     }
        // });
    });



    $('#btn-mail-issue').on('click', function(e) {
        console.log('mail-send');
        var bootbox_promt = bootbox.prompt({
            title: "Расскажите нам об ошибке!",
            inputType: 'textarea',
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times fa-fw"></i> Отмена'
                },
                confirm: {
                    label: '<i class="fa fa-check fa-fw"></i> Отправить'
                }
            },
            callback: function(result) {
                if (result) {
                    var params = {
                        "data": result
                    };

                    $.ajax({
                        url: "/send_email",
                        type: "POST",
                        dataType: "text",
                        contentType: "application/json",
                        data: JSON.stringify(params),

                        complete: function() {
                            show_noty('success', 'Сообщение отправлено')
                        },

                        error: function() {
                            show_noty('alert', 'Сообщение не отправлено')
                        },
                    });
                }
            }
        });
        bootbox_promt.css({
            'top': '50%',
            'margin-top': function() {
                return -(bootbox_promt.height() / 2);
            }
        });
    });


    $("#menu-toggle").on('click', (function() {
        $("#wrapper").toggleClass("toggled");
        $("#wrapper.toggled").find("#sidebar-wrapper").find(".collapse").collapse("hide");

        // setTimeout(function() { console.log('update_view') }, 1000);

        var params = {
            'key': 'menu',
            'value': $(this).hasClass('active') ? "" : "active"
        };

        session_save(params);

        // $(this).button('reset');

    }));


    $('[rel="tooltip"]').tooltip({
        container: 'body',
        placement: 'bottom'
    });

    $('[data-toggle="tooltip"]').tooltip({
        container: 'body'
    });


    $('#ws-status-icon').tooltip({
        container: 'body',
        placement: 'right'
    });

});