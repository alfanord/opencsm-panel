function update_dashboard() {

    $.ajax({
        url: '/_index',
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            $('#fa-list-troubles').replaceWith('<i class="fa fa-circle-o-notch fa-spin fa-lg fa-fw" aria-hidden="true" id="fa-list-troubles"></i>');
        },

        success: function(data) {
            console.log(data);
            var countup_options = {
                useEasing: true,
                useGrouping: true,
                separator: ',',
                decimal: '.'
            };


            $.each(data, function(key, value) {
                // var old_value = $('#' + key).html().replace(',', '');
                // if (old_value != value) {
                var counter = new CountUp(key, 0, value, 0, 1, countup_options);
                counter.start(function() {
                    $('#' + key).parent().filter('a').removeClass('disabled');
                });
                // }

            });

        },
        complete: function() {

            $('#fa-list-troubles').replaceWith('<i class="fa fa-exclamation-triangle fa-lg fa-fw" aria-hidden="true" id="fa-list-troubles"></i>');
        }
    });

};


function update_dash_objects() {

    $.ajax({
        url: '/_index_top_objects',
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            $('#fa-top10-objects').replaceWith('<i class="fa fa-circle-o-notch fa-spin fa-lg fa-fw" aria-hidden="true" id="fa-top10-objects"></i>');
        },

        success: function(data) {
            console.log('asd')
            $('#list-top10-objects').empty();
            $.each(data, function(key, value) {
                $('#list-top10-objects').append('<a href="/objects/' + value.id + '" class="list-group-item">' + value.status_icon + ' <span class="badge">' +
                    value.troubles_count + '</span>' + value.id_str.substr(6) + ' ' + value.title + '</a>');
            });
        },
        complete: function() {
            $('#fa-top10-objects').replaceWith('<i class="fa fa-bar-chart fa-lg fa-fw" aria-hidden="true" id="fa-top10-objects"></i>');

        }
    });
};

// var iconFeatures = [];

// var vectorSource = new ol.source.Vector({
//     features: iconFeatures //add an array of features
// });

// var iconStyle = new ol.style.Style({
//     image: new ol.style.Icon( /** @type {olx.style.IconOptions} */ ({
//         anchor: [0.5, 46],
//         anchorXUnits: 'fraction',
//         anchorYUnits: 'pixels',
//         opacity: 0.75,
//         src: '/static/img/maps-and-flags.png'
//     }))
// });


var troublesLayer = new ol.layer.Vector({
    source: troubles_source
});


var raster = new ol.layer.Tile({
    source: new ol.source.OSM()
});

var map = new ol.Map({
    target: 'dashboard_map',
    controls: ol.control.defaults().extend([
        new ol.control.FullScreen()
    ]),
    // layers: [raster,
    //     new ol.layer.Vector({
    //         source: vectorSource,
    //         style: iconStyle
    //     })
    layers: [raster, troublesLayer],
    view: new ol.View({
        center: ol.proj.fromLonLat([30.384700775146484, 59.93560028076172]),
        zoom: 9
    })
});

function init_troubles() {

    $.ajax({
        url: '/_troubles',
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            // $('#fa-list-troubles').replaceWith('<i class="fa fa-circle-o-notch fa-spin fa-lg fa-fw" aria-hidden="true" id="fa-list-troubles"></i>');
        },

        success: function(data) {
            // vectorSource.clear();
            // iconFeatures = [];
            // for (var i = 0; i < data.length; i++) {
            //     // console.log(data.troubles_list[i]);
            //     tr = data[i];
            //     var iconGeometry = new ol.geom.Point(ol.proj.transform([tr.object.lon, tr.object.lat], 'EPSG:4326', 'EPSG:3857'));
            //     var iconFeature = new ol.Feature({
            //         geometry: iconGeometry
            //     });
            //     // console.log(tr.object.lon);
            //     iconFeatures.push(iconFeature);

            //     //Do something
            // }
            console.log(data);
            // vectorSource.addFeatures(iconFeatures);

            // var countup_options = {
            //     useEasing: true,
            //     useGrouping: true,
            //     separator: ',',
            //     decimal: '.'
            // };


            // $.each(data, function(key, value) {
            //     // var old_value = $('#' + key).html().replace(',', '');
            //     // if (old_value != value) {
            //     var counter = new CountUp(key, 0, value, 0, 1, countup_options);
            //     counter.start(function() {
            //         $('#' + key).parent().filter('a').removeClass('disabled');
            //     });
            //     // }

            // });

        },
        complete: function() {

            // $('#fa-list-troubles').replaceWith('<i class="fa fa-exclamation-triangle fa-lg fa-fw" aria-hidden="true" id="fa-list-troubles"></i>');
        }
    });

};



$(document).ready(function() {

    // init_troubles();


    update_dashboard();
    update_dash_objects();




    socket.on('connect', function() {
        socket.emit('join', { room: 'message' });
        socket.emit('join', { room: 'object' });
    });

    socket.on('db_event', function(payload) {
        // console.log(payload)
        var db_table = payload.table;
        var action = payload.action;
        var data = payload.data;

        if (db_table == 'message' && action == 'INSERT') {

            var q = 'E';

            if (data.qualifier == '1') {
                q = 'E';
            } else {
                q = 'R'
            }

            var panel_code = data.object_code.substr(6, 4);

            var contact_id = q + data.event_code + data.section_code + data.zone_code;
            var channel_title = null;
            var event_type_title = null;

            if (data.channel != null) {
                channel_title = data.channel.title;
            }

            if (data.event_type_json != null) {
                event_type_title = data.event_type_json.title;
            }

            $('#last_message_object_title').html(data.object.status_icon + ' ' + data.object_code.substr(6) + ' ' + data.object.title_shortened);

            $('#last_message_text').html('<i class="fa fa-envelope fa-fw"></i>&nbsp;' + data.text.substr(0, 40) + '..');

            $('#last_message_text').attr('class', function(i, cls) {
                if (/bg_\w+/.test(cls)) {
                    return cls.replace(/bg_\w+/, data.event_type_json.color);
                };

            });
            $("#last_message_object_address").html('<i class="fa fa-map-marker fa-fw"></i>&nbsp;' + data.object.address);

        } else if (db_table == 'trouble' && action == 'INSERT') {
            // show_noty('info', '<h4>' + data.event_type_json.title + '</h4>' + data.object.title, 3000);
            update_dashboard();
            update_dash_objects();
            // init_troubles();
        } else if (db_table == 'trouble' && (action == 'UPDATE' || action == 'DELETE')) {
            // init_troubles();
        } else if (db_table == 'object' && action == 'UPDATE') {

        }

    });


    socket.on('pbx_event', function(data) {
        console.log(data);
        show_noty('info', data.person + ', prio:' + data.prio)
    });

    socket.on('my_response', function(msg) {
        console.log(msg);
    });




    // var iconFeatures = [];

    // var iconFeature = new ol.Feature({
    //     geometry: new ol.geom.Point(ol.proj.transform([lon, lat], 'EPSG:4326',
    //         'EPSG:3857')),
    //     name: 'Null Island',
    //     population: 4000,
    //     rainfall: 500
    // });

    // iconFeatures.push(iconFeature);

    // var vectorSource = new ol.source.Vector({
    //     features: iconFeatures //add an array of features
    // });

    // var iconStyle = new ol.style.Style({
    //     image: new ol.style.Icon( /** @type {olx.style.IconOptions} */ ({
    //         anchor: [0.5, 46],
    //         anchorXUnits: 'fraction',
    //         anchorYUnits: 'pixels',
    //         opacity: 0.75,
    //         src: '/static/img/maps-and-flags.png'
    //     }))
    // });




    // var table = $('#dt_messages_sm').DataTable({
    //     responsive: true,
    //     colReorder: true,
    //     ajax: function(data, callback, settings) {

    //         $.ajax({
    //             url: '/_messages',
    //             type: 'GET',
    //             // data: { date: params.date },
    //             beforeSend: function() {
    //                 // console.log('START');
    //                 // $('i#fa-messages-table').replaceWith('<i class="fa fa-circle-o-notch fa-spin fa-lg fa-fw" aria-hidden="true" id="fa-messages-table"></i>');
    //             },

    //             success: function(data) {
    //                 callback(JSON.parse(data));
    //                 // $(params.table.replace('#', '.') + '-counter').text(table.data().count()); //TODO: update after responsive tab show
    //             },
    //             complete: function() {
    //                 // console.log('COMPLITE');
    //                 // $('i#fa-messages-table').replaceWith('<i class="fa fa-envelope fa-lg fa-fw" aria-hidden="true" id="fa-messages-table"></i>');
    //             }
    //         });
    //     },

    //     lengthMenu: [
    //         [10, 25, 50, 100, -1],
    //         [10, 25, 50, 100, "Все"]
    //     ],

    //     drawCallback: function() {
    //         $('.dataTables_paginate > .pagination').addClass('pagination-sm');

    //     },

    //     // iDisplayLength: 10,
    //     // ordering: true,
    //     // orderFixed: [0, 'asc'],

    //     columns: [
    //         { data: "timestamp", title: "Время", orderable: false },
    //         { data: "event_text", title: "Сообщение", orderable: false, responsivePriority: 10001 },
    //         { data: "panel_code", title: "Объект", orderable: false },
    //         { data: "contact_id", title: "Код", orderable: false },
    //         { data: "object_id", orderable: false, visible: false, searchable: false },
    //         { data: "trouble_id", orderable: false, visible: false, searchable: false },
    //         { data: "event_type", orderable: false, visible: false, searchable: false }
    //     ],
    //     // pageLength: params.pageLength,

    //     dom: '<"panel panel-default"<"#messages-heading.panel-heading"f>t<"panel-footer"p>>',

    //     createdRow: function(row, data, index) {
    //         // if (data[5].replace(/[\$,]/g, '') * 1 > 150000) {
    //         // $('td', row).eq(5).addClass('highlight');
    //         // }
    //         // $(this).columns.adjust().responsive.recalc();
    //         // $('td', row).eq(1).text($('td', row).eq(1).text().substring(0, 48) + '...');
    //         $(row).addClass(event_types_map[data.event_type]);
    //         console.log();
    //     },

    //     language: {
    //         lengthMenu: "_MENU_  записей на странице",
    //         info: "Страница _PAGE_ из _PAGES_",
    //         infoFiltered: "(отфильтровано из _MAX_ записей)",
    //         search: "Фильтр:",
    //         zeroRecords: "Ничего не найдено :(",
    //         infoEmpty: "0 из 0 записей",
    //         paginate: {
    //             first: "Начало",
    //             last: "Конец",
    //             next: "След.",
    //             previous: "Пред."
    //         }
    //     }

    // });

    // table.order.fixed({ pre: [0, 'desc'] }).draw();

    // $('#dt_messages thead tr').hide();

    // $('#messages-heading').prepend('<div class="col-lg-1 messages-icon-wrapper"><i class="fa fa-envelope fa-lg fa-fw" aria-hidden="true" id="fa-messages-table"></i></div>');


});