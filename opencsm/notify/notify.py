# -*- coding: utf-8 -*-
# noinspection SpellCheckingInspection

import asyncio
import json
import aiopg
import re
from opencsm import app, ws
from opencsm.models import Object, EventType, Trouble, Channel
from shapely import wkb


# import colorlog
import logging

class DatabasePayload(object):
    action = None
    data = None
    table = None

    def __init__(self, j):
        self.__dict__ = json.loads(j)
        self.init()

    def init(self):
        with app.app_context():
            ev_id = self.data.get('event_type')
            ev = EventType.query.get(ev_id) if ev_id is not None else None
            self.data.update({'event_type_json': ev.to_dict if ev is not None else None})

            obj_id = self.data.get('object_id')
            obj = Object.query.get(obj_id) if obj_id is not None else None
            self.data.update({'object': obj.to_dict if obj is not None else None})

            tr_id = self.data.get('trouble_id', None)
            tr = Trouble.query.get(tr_id) if tr_id is not None else None
            self.data.update({'trouble': tr.to_dict if tr is not None else None})

            ch_id = self.data.get('channel_id', None)
            ch = Channel.query.get(ch_id) if ch_id is not None else None
            self.data.update({'channel': ch.to_dict if ch is not None else None})

            geom = self.data.get('geom', None)

            if geom is not None:
                point = wkb.loads(geom, hex=True)
                self.data.update({'lon': point.x if point is not None else None})
                self.data.update({'lat': point.y if point is not None else None})
                self.data.update({'geom_wkt': point.to_wkt() if point is not None else None})

    @property
    def to_dict(self):
        return self.__dict__


class Notify(object):
    channel = ''
    loop = None

    def __init__(self):
        self.channel = app.config['PG_NOTIFY_CHANNEL']
        dsn = re.split('://|:|@|/', app.config['SQLALCHEMY_DATABASE_URI'])
        self.dsn = 'host={} port={} user={} password={} dbname={}'.format(dsn[3], dsn[4], dsn[1], dsn[2], dsn[5])

    async def listen(self, conn):
        async with conn.cursor() as cur:
            await cur.execute("LISTEN {}".format(self.channel))
            while True:
                msg = await conn.notifies.get()
                if msg.payload == 'finish':
                    return
                else:
                    payload = DatabasePayload(msg.payload)
                    if ws.check_room(payload.table):
                        ws.bg_emit(payload.table, 'db_event', payload.to_dict)
                        # print(payload.to_dict)

    async def main(self):
        async with aiopg.create_pool(self.dsn) as pool:
            async with pool.acquire() as conn1:
                listener = self.listen(conn1)
                await asyncio.gather(listener)

    def start(self):
        self.loop = asyncio.new_event_loop()
        self.loop.run_until_complete(self.main())
        self.loop.close()
