from flask_wtf import FlaskForm
from flask_wtf.recaptcha import RecaptchaField
from wtforms import StringField, PasswordField, Label, SelectField
from wtforms.validators import DataRequired


class LoginForm(FlaskForm):
    l_username = Label('username', 'Login')
    l_password = Label('password', 'Password')
    username = StringField('username', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])
    recaptcha = RecaptchaField()


class SearchForm(FlaskForm):
    search = StringField('search', validators=[DataRequired()])
