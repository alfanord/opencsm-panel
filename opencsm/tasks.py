# -*- coding: utf-8 -*-
# noinspection SpellCheckingInspection

import os
from flask_whooshalchemyplus import index_all
from flask_mail import Message
from opencsm import app, db, lm, socketio, mail, celery, cache
import colorlog
import logging

@celery.task(name='tasks.add')
def reindex_search_db():
    with app.app_context():
        logging.info('Reindexing Search DB')
        whoosh_basedir = os.path.join(app.config['WHOOSH_BASE'])
        for directory in os.scandir(whoosh_basedir):
            if directory.is_dir():
                logging.info('Removing {} search index'.format(directory.name))
                for file in os.scandir(directory.path):
                    os.remove(file.path)
                os.rmdir(directory.path)
        index_all(app)


@celery.task
def send_async_email(payload):
    with app.app_context():
        msg = Message('Think Async', recipients=['rs@an-security.ru'])
        msg.body = payload['data']
        msg.subject = 'Ошибка в панели'
        mail.send(msg)
