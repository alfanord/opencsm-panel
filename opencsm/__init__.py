# -*- coding: utf-8 -*-

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_socketio import SocketIO
from flask_mail import Mail
from flask_caching import Cache
from flask_principal import Principal
from opencsm.avatar import Avatar
from flask_thumbnails import Thumbnail
from celery import Celery
import eventlet
import colorlog
import logging

eventlet.monkey_patch()

colorlog.basicConfig(format='[%(asctime)s %(log_color)s%(levelname)-7s:%(name)s] %(message)s', level=logging.DEBUG)

app = Flask(__name__)
app.config.from_object('config.Config')
app.logger.addHandler(colorlog)

cache = Cache(app)
principals = Principal(app)
db = SQLAlchemy(app)

mail = Mail(app)
# socketio = SocketIO(app, async_mode='eventlet', logger=colorlog, engineio_logger=colorlog)
socketio = SocketIO(app, async_mode='eventlet', cors_allowed_origins=app.config['ORIGINS'], logger=colorlog, engineio_logger=colorlog)
celery = Celery(app.name, backend=app.config['CELERY_RESULT_BACKEND'], broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)
lm = LoginManager()
lm.init_app(app)
lm.login_view = 'login'
# thumb = Thumbnail(app)
Avatar(app)

from opencsm import views, models
from opencsm.websocket import WebSocket


ws = WebSocket('/ocsm')
socketio.on_namespace(ws)

from opencsm.notify import Notify
from opencsm.asterisk import Asterisk

pgnotify = Notify() if app.config['PG_NOTIFY_ENABLED'] else None
pbx = Asterisk() if app.config['ARI_ENABLED'] else None

