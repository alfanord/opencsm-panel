# -*- coding: utf-8 -*-
# noinspection SpellCheckingInspection

from flask_login import current_user
from flask_principal import RoleNeed, ActionNeed, UserNeed, Permission, identity_loaded
from opencsm import app, principals

admin_permission = Permission(RoleNeed('admin'))
calls_permission = Permission(RoleNeed('calls'))
analytics_permission = Permission(RoleNeed('analytics'))
map_permission = Permission(RoleNeed('map'))
trouble_permission = Permission(RoleNeed('trouble'))

permissions = {'admin': admin_permission

               }


@identity_loaded.connect_via(app)
def on_identity_loaded(sender, identity):
    # Set the identity user object
    identity.user = current_user

    # Add the UserNeed to the identity
    if hasattr(current_user, 'id'):
        identity.provides.add(UserNeed(current_user.id))

    # Assuming the User model has a list of roles, update the
    # identity with the roles that the user provides
    if hasattr(current_user, 'perms'):
        for role in current_user.perms:
            identity.provides.add(RoleNeed(role))
