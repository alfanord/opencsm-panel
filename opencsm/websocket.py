# -*- coding: utf-8 -*-
# noinspection SpellCheckingInspection

from flask_socketio import emit, join_room, leave_room, close_room, rooms, disconnect, Namespace
from flask_login import login_user, logout_user, current_user, login_required
from flask import render_template, flash, redirect, session, url_for, request, g, jsonify

from opencsm import app

import logging


#
# def bg_emit(room, event, data):
#     socketio.emit(event, data, room=room, namespace='/ocsm')


# @socketio.on('connect', namespace='/ocsm')
# @login_required
# def connect():
#     logging.debug('Client connected to NS {} sid {}'.format(request.namespace, request.sid))
#     emit('my_response', {'data': 'Connected', 'count': 0})
#
#
# @socketio.on('join', namespace='/ocsm')
# @login_required
# def join(message):
#     if 'room' in message:
#         join_room(message['room'])
#         session['receive_count'] = session.get('receive_count', 0) + 1
#         emit('my_response',
#              {'data': 'In rooms: ' + ', '.join(rooms()),
#               'count': session['receive_count']})
#     else:
#         logging.error(message)


# @socketio.on('leave', namespace='/ocsm')
# def leave(message):
#     if 'room' in message:
#         leave_room(message['room'])
#         session['receive_count'] = session.get('receive_count', 0) + 1
#         emit('my_response',
#              {'data': 'In rooms: ' + ', '.join(rooms()),
#               'count': session['receive_count']})
#     else:
#         logging.error(message)

#
# @socketio.on('disconnect_request', namespace='/ocsm')
# @login_required
# def disconnect_request():
#     emit('my_response', {'data': 'Disconnected!', 'count': session['receive_count']})
#     disconnect()
#
#
# @socketio.on('disconnect', namespace='/ocsm')
# @login_required
# def disconnect():
#     logging.debug('Client disconnected from NS {} sid {}'.format(request.namespace, request.sid))


class WebSocket(Namespace):
    active_rooms = dict()

    @login_required
    def on_connect(self):
        logging.debug('Client connected to NS {} sid {}'.format(request.namespace, request.sid))
        emit('my_response', {'rooms': rooms(), 'event': 'connect', 'count': 0})

    @login_required
    def on_disconnect_request(self):
        session['receive_count'] = session.get('receive_count', 0) + 1
        emit('my_response', {'rooms': rooms(), 'event': 'disconnect', 'count': session['receive_count']})
        self.on_disconnect()

    @login_required
    def on_disconnect(self):
        for room in self.active_rooms:
            if request.sid in self.active_rooms[room]:
                self.active_rooms[room].remove(request.sid)
        logging.debug('Client disconnected from NS {} sid {}'.format(request.namespace, request.sid))

    @login_required
    def on_leave(self, message):
        if 'room' in message:
            room = message['room']
            leave_room(room)
            if room not in self.active_rooms:
                return
            else:
                if request.sid in self.active_rooms[room]:
                    self.active_rooms[room].remove(request.sid)
                session['receive_count'] = session.get('receive_count', 0) + 1
                emit('my_response',
                     {'rooms': rooms(),
                      'count': session['receive_count']})
        else:
            logging.error(message)

    @login_required
    def on_join(self, message):
        if 'room' in message:
            room = message['room']
            join_room(room)
            if room not in self.active_rooms:
                self.active_rooms[room] = list()
            self.active_rooms[room].append(request.sid)
            session['receive_count'] = session.get('receive_count', 0) + 1
            emit('my_response',
                 {'rooms': rooms(),
                  'count': session['receive_count']})
        else:
            logging.error(message)

    @staticmethod
    def bg_emit(room, event, data):
        with app.app_context():
            emit(event, data, room=room, namespace='/ocsm')

    def check_room(self, room):
        return False if room not in self.active_rooms or len(self.active_rooms[room]) < 1 else True
