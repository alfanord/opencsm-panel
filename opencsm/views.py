# -*- coding: utf-8 -*-
# noinspection SpellCheckingInspection

import json
import os
import collections
import itertools
import operator
from urllib import parse
from textwrap import shorten
from functools import singledispatch

from flask import render_template, flash, redirect, session, url_for, request, g, jsonify, current_app
from flask_login import login_user, logout_user, current_user, login_required
from flask_principal import Identity, AnonymousIdentity, identity_changed

from werkzeug.utils import secure_filename

from opencsm import app, db, lm, cache
from opencsm.forms import LoginForm, SearchForm
from opencsm.models import Chop, Object, MobileGroup, Person, WialonUnit, User, Cdr, Perms, EventType, Event, Trouble, Message
from opencsm.tasks import send_async_email
from opencsm.principals import *

from hashlib import md5
from datetime import datetime, timedelta
from dateutil import parser


from functools import wraps
from sqlalchemy import cast, DateTime, Date
from sqlalchemy.dialects.postgresql import DATE, TIMESTAMP
import logging


# def required_perms(*perms):
#     def wrapper(f):
#         @wraps(f)
#         def wrapped(*args, **kwargs):
#             # if UserRole(g.user.role) not in roles:
#             if not any(map(lambda v: v in g.user.perms, perms)):
#                 flash('Доступ запрещён!', 'error')
#                 return redirect(url_for('index'))
#             return f(*args, **kwargs)
#
#         return wrapped
#
#     return wrapper


# @app.route('/csp-report', methods=['POST'])
# @login_required
# def csp_report():
#     if request.get_json():
#         logging.debug('CSP Report = {}'.format(request.get_json()))
#     return '', 200


@app.errorhandler(403)
def authorization_failed(e):
    flash('Your current identity is {id}. You need special privileges to access this page'.format(id=g.identity.id), 'error')

    return render_template('error.html')


# @app.route('/_session', methods=['POST'])
# @login_required
# def _session():
#     params = request.get_json()
#     session.update({params['key']: params['value']})
#     logging.debug('Added params {} to session'.format(params))
#     return '', 200


@app.before_request
def before_request():
    g.user = current_user
    if g.user.is_authenticated:
        g.user.last_seen = datetime.now()
        db.session.add(g.user)
        db.session.commit()
        g.search_form = SearchForm()
        # g.user_session = session


@lm.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


@lm.unauthorized_handler
def unauthorized():
    logging.warning("Unauthorized {} from {}".format(request, request.remote_addr))
    return redirect(url_for('login'))


# @app.route('/search', methods=['POST'])
# @login_required
# def search():
#     if not g.search_form.validate_on_submit():
#         return redirect(url_for('index'))
#     # return redirect(url_for('search_results', query=g.search_form.search.data))
#     query = parse.quote(g.search_form.search.data)
#     return redirect(url_for('search_results', query=query))
#
#
# @app.route('/search_results/<string:query>')
# @login_required
# def search_results(query):
#     query_string = parse.unquote(query)
#     if query_string.isdigit():
#         query_string = '*{}'.format(query_string)
#         results = Object.query.whoosh_search(query_string, fields=('object_id_str',)).order_by(Object.id).all()
#     else:
#         query_string = '*{}*'.format(query_string)
#         results = Object.query.whoosh_search(query_string).order_by(Object.id).all()
#     return render_template('search_results.html', title='search_results', query=query, results=results)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user is not None and current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()

    if request.method == 'GET':
        return render_template('login.html', title="Вход", form=form)

    username = form.username.data
    password = form.password.data

    hash_obj = md5(password.encode())
    m_pwd = (hash_obj.hexdigest())

    registered_user = User.query.filter_by(username=username, password=m_pwd).first()
    if registered_user is None:
        flash('Неправильный пароль или логин!', 'warning')
        return redirect(url_for('login'))
    login_user(registered_user, remember=True)
    identity_changed.send(app, identity=Identity(registered_user.id))

    logging.info('User {} logged in with perms={}'.format(registered_user.username, registered_user.perms))
    # flash("Добро пожаловать, {}!".format(registered_user.name), 'success')
    session['menu'] = 'active'
    return redirect(request.args.get('next') or url_for('calls'))


@app.route('/logout')
@login_required
def logout():
    logout_user()
    for key in ('identity.name', 'identity.auth_type'):
        session.pop(key, None)

    identity_changed.send(app, identity=AnonymousIdentity())
    return redirect(url_for('login'))


# @app.route('/troubles/e/<int:event_type_id>')
# @login_required
# # @cache.cached(timeout=20)
# def trouble_by_ev(event_type_id):
#     event = EventType.query.get(event_type_id)
#     recent_troubles = dict(current_user.recent_troubles)
#     recent_troubles_filtered = list()
#     if event in recent_troubles.keys():
#         recent_troubles_filtered = recent_troubles[event]
#     return render_template('troubles.html', recent_troubles=recent_troubles_filtered, title='troubles')
#
#
# @app.route('/troubles/<int:trouble_id>')
# @login_required
# # @cache.cached(timeout=20)
# def trouble_by_id(trouble_id):
#     trouble = Trouble.query.get(trouble_id)
#     # recent_troubles = dict(current_user.recent_troubles)
#     # recent_troubles_filtered = list()
#     # if event in recent_troubles.keys():
#     #     recent_troubles_filtered = recent_troubles[event]
#     return render_template('trouble.html', trouble=trouble, object=trouble.object, title='troubles')


@app.route('/')
@app.route('/index')
@login_required
# @cache.cached(30)
def index():
    # recent_troubles = current_user.recent_troubles
    # last_message = Message.query.order_by(Message.id.desc()).first()
    # event_types = EventType.query.filter_by(is_incident=1).order_by(EventType.priority.desc()).all()
    # return render_template('index.html', last_message=last_message, event_types=event_types, title='index')
    return render_template('calls.html', title='voip_calls', acl=Perms.analytics)


# @app.route('/_index', methods=['GET'])
# @login_required
# # @required_perms(Perms.calls)
# def _index():
#     recent_troubles = dict(current_user.recent_troubles_cached)
#     data = dict()
#     total_events = 0
#     for event_type, trouble_list in recent_troubles.items():
#         key_name = 'event_type_{}'.format(event_type.id)
#         list_length = len(trouble_list)
#         data.update({key_name: list_length})
#         total_events = total_events + list_length
#
#     # total_troubles = Trouble.query.filter(Trouble.opened == 1).all()
#     total_active = Object.query.filter(Object.status == 3).count()
#     total_repair = Object.query.filter(Object.status == 2).count()
#     total_stopped = Object.query.filter(Object.status == 1).count()
#     total_disabled = Object.query.filter(Object.status == 0).count()
#     total_persons = Person.query.count()
#     total_mobile_groups = MobileGroup.query.count()
#
#     # tr_list = []
#     # for tr in total_troubles:
#     #     tr_list.append(tr.to_dict)
#
#     data.update({
#         # 'total_objects': current_user.total_objects,
#         'total_objects': total_active,
#         # 'total_wialons': WialonUnit.query.count(),
#         'total_wialons': total_repair,
#         # 'total_gbrs': MobileGroup.query.count(),
#         'total_gbrs': total_stopped,
#         # 'total_chops': Chop.query.count(),
#         'total_chops': total_disabled,
#         # 'total_troubles': len(total_troubles),
#         # 'tr_list': tr_list,
#         'total_events': total_events,
#         'total_mobile_groups': total_mobile_groups,
#         'total_persons': total_persons
#     })
#     return jsonify(data)


# @app.route('/_index_top_objects', methods=['GET'])
# @login_required
# # @cache.cached(timeout=30)
# # @required_perms(Perms.calls)
# def _index_top_objects():
#     recent_troubles = dict(current_user.recent_troubles_cached)
#     data = dict()
#     data_top10 = list()
#     for event_type, trouble_list in recent_troubles.items():
#         for trouble in trouble_list:
#             if trouble.object_id not in data:
#                 data[trouble.object_id] = 0
#             data[trouble.object_id] += 1
#     sorted_x = collections.OrderedDict(sorted(data.items(), key=operator.itemgetter(1), reverse=True))
#
#     x = itertools.islice(sorted_x.items(), 0, 10)
#
#     for key, value in x:
#         tmp_object = Object.query.get(key)
#         tmp_data = {'id': tmp_object.id,
#                     'id_str': tmp_object.object_id_str,
#                     'title': tmp_object.title_shortened,
#                     'status_icon': tmp_object.status_icon,
#                     'troubles_count': value
#                     }
#         data_top10.append(tmp_data)
#
#     return jsonify(data_top10)


# @app.route('/_index_top_mobile_groups', methods=['GET'])
# @login_required
# # @cache.cached(timeout=30)
# # @required_perms(Perms.calls)
# def _index_top_mobile_groups():
#     trouble_list = current_user.troubles_list
#     data = dict()
#     data_top10 = list()
#     mobile_groups = list()
#     # for event_type, trouble_list in recent_troubles.items():
#
#     # for trouble in trouble_list:
#     # trouble_ids.append(trouble.id)
#     # print(Trouble.query.get(t).mobile_groups)
#     # print(trouble.mobile_groups)
#     # print(trouble.mobile_groups.count())
#     # if trouble.mobile_groups.count() > 0:
#     # if trouble.mobile_groups.count() > 0:
#     # all_groups = trouble.mobile_groups
#     # print(all)
#     # mobile_groups.extend(all_groups)
#     # data[trouble.object_id] = 0
#     # data[trouble.object_id] += 1
#     # print(trouble_ids)
#     # for t in trouble_ids:
#     #     print(Trouble.query.get(t).mobile_groups)
#
#     # chop_troubles = Trouble.query.filter(Trouble.opened == 1, Trouble.object_id.in_(chop.monitored_objects.with_entities(Object.id))).all()
#     # sorted_x = collections.OrderedDict(sorted(data.items(), key=operator.itemgetter(1), reverse=True))
#     #
#     # x = itertools.islice(sorted_x.items(), 0, 10)
#     #
#     # for key, value in x:
#     #     tmp_object = Object.query.get(key)
#     #     tmp_data = {'id': tmp_object.id,
#     #                 'id_str': tmp_object.object_id_str,
#     #                 'title': tmp_object.title,
#     #                 'troubles_count': value
#     #                 }
#     #     data_top10.append(tmp_data)
#     print(len(mobile_groups))
#     return jsonify(data_top10)


# @app.route('/analytics')
# @login_required
# @analytics_permission.require(http_exception=403)
# def analytics():
#     return render_template('blank.html', title='analytics')
#
#
# @app.route('/settings')
# @login_required
# @admin_permission.require(http_exception=403)
# def settings():
#     return render_template('settings.html', title='settings')


@app.route('/calls')
@login_required
@calls_permission.require(http_exception=403)
# @cache.cached(300)
def calls():
    return render_template('calls.html', title='voip_calls', acl=Perms.analytics)


# @app.route('/map')
# @login_required
# @map_permission.require(http_exception=403)
# def view_map():
#     return render_template('map.html', title='map')
#
#
# @app.route('/objects')
# @login_required
# @cache.cached(timeout=60)
# def objects():
#     results = Object.query.all()
#     return render_template('search_results.html', results=results, title='objects')


@singledispatch
def to_serializable(val):
    """Used by default."""
    return str(val)


@to_serializable.register(datetime)
def ts_datetime(val):
    """Used if *val* is an instance of datetime."""
    return val.isoformat() + "Z"


# @app.route('/_objects/<int:id>', methods=['GET'])
# @login_required
# # @required_perms(Perms.calls)
# def _objects(id):
#     o = Object.query.get(id)
#
#     if not o:
#         return '', 400
#     else:
#         return json.dumps(o.to_json, default=to_serializable)
#         # else:
#
#
# @app.route('/_mobile_groups', methods=['GET'])
# @login_required
# # @required_perms(Perms.calls)
# def _mobile_groups():
#     results = []
#     mm = MobileGroup.query.filter(MobileGroup.wialon_unit).all()
#     for m in mm:
#         results.append(m.to_dict)
#     return jsonify(results)
#
#
# @app.route('/_troubles', methods=['GET'])
# @login_required
# # @required_perms(Perms.calls)
# def _troubles():
#     troubles_list = []
#     troubles = Trouble.query.filter(Trouble.opened == 1).order_by(Trouble.id.asc()).all()
#     for t in troubles:
#         troubles_list.append(t.to_dict_full)
#     return jsonify(troubles_list)


# @app.route('/_messages', methods=['GET'])
# @login_required
# # @required_perms(Perms.calls)
# def _messages():
#     if 'date_start' in request.args:
#         date_start = parser.parse(request.args.get('date_start'))
#     else:
#         date_start = datetime.now() - timedelta(hours=6)
#
#     if 'date_end' in request.args:
#         date_end = parser.parse(request.args.get('date_end'))
#     else:
#         date_end = datetime.now()
#
#     if 'object_id' in request.args and request.args.get('object_id', '') != '':
#         messages_q = Message.query.filter(Message.object_id == request.args.get('object_id'),
#                                           Message.timestamp > date_start,
#                                           Message.timestamp < date_end).order_by(Message.id.desc()).limit(1000)
#     else:
#         messages_q = Message.query.filter(Message.timestamp > date_start,
#                                           Message.timestamp < date_end).order_by(Message.id.desc()).limit(100)
#
#     if request.args.get('trouble_id', '') != '':
#         messages_q = Message.query.filter(Message.trouble_id == request.args.get('trouble_id'))
#
#     messages = messages_q.all()
#     messages_list = []
#     for message in messages:
#         message_object = message.object
#         message_dict = {
#             'timestamp': message.human_date,
#             'event_type_title': message.event_type.type_ru if message.event_type else None,
#             'channel_title': message.channel.title if message.channel else None,
#             'object_title': '{} {}'.format(message_object.status_icon, message_object.title),
#             'object_address': message_object.address,
#             'event_text': message.event_text,
#             'user_comment': message.user_comment,
#             'panel_code': message.panel_code,
#             'event_code': message.event_code,
#             'partition_code': message.partition_code,
#             'zone_code': message.zone_code,
#             # 'contact_id': message.contact_id,
#             'object_id': message.object_id,
#             'trouble_id': message.trouble_id,
#             'event_type': message.event_type_id
#         }
#         messages_list.append(message_dict)
#     return json.dumps({'data': messages_list})
# else:
#     return '', 400


# @app.route('/_daily_chart', methods=['GET'])
# @login_required
# # @required_perms(Perms.calls)
# def _daily_chart():
#     data = [
#         {'y': '01:00', 'a': 75},
#         {'y': '02:00', 'a': 50},
#         {'y': '03:00', 'a': 75},
#         {'y': '04:00', 'a': 50},
#         {'y': '05:00', 'a': 75},
#         {'y': '06:00', 'a': 100},
#         {'y': '07:00', 'a': 66},
#         {'y': '08:00', 'a': 35},
#         {'y': '09:00', 'a': 50},
#         {'y': '10:00', 'a': 75},
#         {'y': '11:00', 'a': 87},
#         {'y': '12:00', 'a': 54},
#         {'y': '13:00', 'a': 87},
#         {'y': '14:00', 'a': 23},
#         {'y': '15:00', 'a': 33},
#         {'y': '16:00', 'a': 49},
#         {'y': '17:00', 'a': 56},
#         {'y': '18:00', 'a': 98},
#         {'y': '19:00', 'a': 12},
#         {'y': '20:00', 'a': 67},
#         {'y': '21:00', 'a': 77},
#         {'y': '22:00', 'a': 72},
#         {'y': '23:00', 'a': 75},
#         {'y': '24:00', 'a': 100},
#     ]
#     return jsonify(data)


@app.route('/_calls_incoming', methods=['GET'])
@login_required
@calls_permission.require(http_exception=403)
def _calls_incoming():
    if 'date' in request.args:
        curr_date = request.args.get('date')

        call_records = Cdr.query.filter(Cdr.dstchannel.op('~')('SIP/95(7|8|9)') |
                                        Cdr.dstchannel.op('~')('SIP/10(1|2|3)'),
                                        Cdr.filename != None,
                                        cast(Cdr.calldate, DATE) == curr_date).order_by(Cdr.calldate.desc())
        calls_list = []
        for call_record in call_records:
            temp_call = {
                'timestamp': str(call_record.calldate.time()),
                'kto': call_record.src,
                'kuda': '<span class="label label-primary lb-sm">{}</span>'.format(call_record.dstchannel.split('/')[1][:3]),
                'record': '<audio class="take-all-space" src="/static/mp3/{}.ogg" preload="none" controls></audio>'.format(call_record.filename)
                # 'duration': str(timedelta(seconds=call_record.duration))
            }
            calls_list.append(temp_call)
        return json.dumps({'data': calls_list})
    else:
        return '', 400


@app.route('/_calls_outgoing', methods=['GET'])
@login_required
@calls_permission.require(http_exception=403)
def _calls_outgoing():
    if 'date' in request.args:
        curr_date = request.args.get('date')
        call_records = Cdr.query.filter(Cdr.channel.op('~')('SIP/95(7|8|9)') |
                                        Cdr.channel.op('~')('SIP/10(1|2|3)'),
                                        Cdr.filename != None,
                                        cast(Cdr.calldate, DATE) == curr_date).order_by(Cdr.calldate.desc())
        calls_list = []
        for call_record in call_records:
            temp_call = {
                # 'record': call_record.uniqueid,
                'timestamp': str(call_record.calldate.time()),
                'kto': '<span class="label label-primary lb-sm">{}</span>'.format(call_record.channel.split('/')[1][:3]),
                'kuda': call_record.dst,
                'record': '<audio class="audiop" src="/static/mp3/{}.ogg" preload="none" controls></audio>'.format(call_record.filename),
                # 'duration': str(timedelta(seconds=call_record.duration))
            }
            calls_list.append(temp_call)
        return json.dumps({'data': calls_list})
    else:
        return '', 400


@app.route('/_calls_unanswered', methods=['GET'])
@login_required
@calls_permission.require(http_exception=403)
def _calls_unanswered():
    if 'date' in request.args:
        curr_date = request.args.get('date')
        call_records = Cdr.query.filter(Cdr.channel.op('~')('SIP/95(7|8|9)'),
                                        Cdr.disposition != 'ANSWERED',
                                        Cdr.disposition != 'BUSY',
                                        cast(Cdr.calldate, DATE) == curr_date).distinct(Cdr.calldate, Cdr.src, Cdr.uniqueid).order_by(Cdr.calldate.desc())
        calls_list = []
        for call_record in call_records:
            temp_call = {
                # 'record': call_record.uniqueid,
                'timestamp': str(call_record.calldate.time()),
                'kto': '<span class="label label-primary lb-sm">{}</span>'.format(call_record.channel.split('/')[1][:3]),
                'kuda': call_record.dst,
                'record': '<span class="label label-danger lb-sm">{}</span>'.format(call_record.disposition)
            }
            calls_list.append(temp_call)
        return json.dumps({'data': calls_list})
    else:
        return '', 400


@app.route('/send_email', methods=['POST'])
@login_required
def send_email():
    payload = request.get_json()
    send_async_email.delay(payload)
    return '', 200


@app.route('/get_ogg', methods=['POST'])
def get_ogg():

    upload_key = request.headers.get('UPLOAD_KEY')

    print(upload_key)
    print(app.config['UPLOAD_KEY'])

    if upload_key != app.config['UPLOAD_KEY']:
        return 'Bad Auth', 401

    if 'file' not in request.files:
        return 'No File', 200

    file = request.files['file']

    if file.filename == '':
        return 'No FileName', 200

    file_name, extension = os.path.splitext(file.filename)

    if file and extension == '.ogg':
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        return 'Saved {}\n'.format(filename), 200

# @app.route('/objects/s/<int:status>')
# @login_required
# @cache.cached(timeout=60)
# def object_by_status(status):
#     results = Object.query.filter_by(status=status).limit(100).all()
#     return render_template('search_results.html', results=results, query=status, title='objects')


# @app.route('/objects/<int:object_id>')
# @login_required
# @cache.cached(timeout=60)
# def object_by_id(object_id):
#     current_object = Object.query.get(object_id)
#     if current_object is not None:
#         return render_template('object.html', title=str(object_id), object=current_object)
#     else:
#         return '', 400

# @app.route('/_make_alarm', methods=['POST'])
# @login_required
# def _make_alarm():
#     json_dict = request.get_json()
#     key = json_dict['code'][-10:]
#     b2b_object = Object.query.get(key)
#     if not b2b_object:
#         print('no object {}'.format(key))
#         return '', 400
#     if not b2b_object.alarm():
#         return '', 400
#     return '', 200
