# -*- coding: utf-8 -*-
from opencsm import app, db, cache
from config import Config
from hashlib import md5
from enum import IntEnum
from flask import jsonify
from sqlalchemy.dialects.postgresql import ARRAY, NUMERIC, JSON, JSONB, TIME, REAL
from geoalchemy2 import Geometry, shape
from shapely.geometry import LineString
import requests
import collections
from datetime import datetime, date, time, timedelta
import pytz
import flask_whooshalchemyplus
import locale
import json
from functools import singledispatch
from textwrap import shorten
from urllib import parse
from opencsm.principals import map_permission
import jsonpickle


# from flask_login import current_user
# import json
# from shapely import wkt, wkb
#
# from shapely.geometry import Point, MultiPolygon
# from binascii import unhexlify
# from threading import Event as ThrEvent
# import phonenumbers
# from phonenumbers.phonenumberutil import NumberParseException

@singledispatch
def to_serializable(val):
    """Used by default."""
    return str(val)


@to_serializable.register(datetime)
def ts_datetime(val):
    """Used if *val* is an instance of datetime."""
    return val.isoformat() + "Z"


class WorkStatus(IntEnum):
    DISABLED = 0
    TESTING = 1
    SERVICE = 2
    ACTIVE = 3


event_types_map = {
    1: 'bg_Red',
    2: 'bg_Red',
    3: 'bg_Magenta',
    4: 'bg_Green',
    5: 'bg_CornflowerBlue',
    6: 'bg_NavajoWhite',
    7: 'bg_Coral',
    8: 'bg_Coral',
    9: 'bg_None',
    10: 'bg_Coral',
    11: 'bg_Yellow',
    12: 'bg_CornflowerBlue',
    13: 'bg_Green',
    14: 'bg_None',
    15: 'bg_None',
    16: 'bg_None'
}

event_colors = {
    1: 'bg-red',
    2: 'bg-red',
    3: 'bg-magenta',
    4: 'bg-green',
    5: 'bg-cornflowerblue',
    6: 'bg-navajowhite',
    7: 'bg-coral',
    8: 'bg-coral',
    9: 'bg-none',
    10: 'bg-coral',
    11: 'bg-yellow',
    12: 'bg-cornflowerblue',
    13: 'bg-green',
    14: 'bg-none',
    15: 'bg-none',
    16: 'bg-none'
}

event_text_colors = {
    1: 'text-red',
    2: 'text-red',
    3: 'text-magenta',
    4: 'text-green',
    5: 'text-cornflowerblue',
    6: 'text-navajowhite',
    7: 'text-coral',
    8: 'text-coral',
    9: 'text-none',
    10: 'text-coral',
    11: 'text-yellow',
    12: 'text-cornflowerblue',
    13: 'text-green',
    14: 'text-none',
    15: 'text-none',
    16: 'text-none'
}


class EnumPerms(set):
    def __getattr__(self, name):
        if name in self:
            return name
        return None


Perms = EnumPerms(['calls', 'analytics', 'settings'])

chop_users = db.Table('chop_users',
                      db.Column('chop_id', db.Integer, db.ForeignKey('chop.id'), nullable=False),
                      db.Column('user_id', db.Integer, db.ForeignKey('ansecuser.id'), nullable=False),
                      db.PrimaryKeyConstraint('chop_id', 'user_id'))

object_gbrs = db.Table('object_gbrs',
                       db.Column('object_id', db.Integer, db.ForeignKey('objects.id'), nullable=False),
                       db.Column('gbr_id', db.Integer, db.ForeignKey('mobile_groups.id'), nullable=False),
                       db.PrimaryKeyConstraint('object_id', 'gbr_id'))

object_persons = db.Table('object_persons',
                          db.Column('object_id', db.Integer, db.ForeignKey('objects.id'), nullable=False),
                          db.Column('person_id', db.Integer, db.ForeignKey('persons.id'), nullable=False),
                          db.PrimaryKeyConstraint('object_id', 'person_id'))

# trouble_messages = db.Table('troublecause',
#                             db.Column('trouble_id', db.Integer, db.ForeignKey('trouble.id'), nullable=False),
#                             db.Column('open_id', db.Integer, db.ForeignKey('message.id'), nullable=False),
#                             db.Column('close_id', db.Integer, db.ForeignKey('message.id'), nullable=True),
#                             db.PrimaryKeyConstraint('trouble_id', 'open_id'))

trouble_gbrs = db.Table('trouble_gbrs',
                        db.Column('trouble_id', db.Integer, db.ForeignKey('troubles.id'), nullable=False),
                        db.Column('gbr_id', db.Integer, db.ForeignKey('mobile_groups.id'), nullable=False),
                        db.PrimaryKeyConstraint('trouble_id', 'gbr_id'))


class Acl(db.Model):
    __tablename__ = 'acl'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(63))
    perms = db.Column(ARRAY(db.String))

    users = db.relationship('User')

    def __repr__(self):
        return '<Acl {}>'.format(self.perms)


class Channel(db.Model):
    __tablename__ = 'ansecline'

    id = db.Column('id_', db.Integer, primary_key=True)
    _title = db.Column('title', db.String(255))

    messages = db.relationship('Message', backref='channel', lazy='dynamic')

    @property
    def title(self):
        return self._title.strip() if self._title is not None else None

    @property
    def to_json(self):
        target = self.__dict__
        target.update({'title': self.title})
        return json.dumps(target, default=to_serializable)

    @property
    def to_dict(self):
        return json.loads(self.to_json)

    def __repr__(self):
        return '<Channel {}>'.format(self.title)


class Cdr(db.Model):
    __tablename__ = 'cdr'

    id = db.Column(db.Integer, primary_key=True)
    calldate = db.Column(db.DateTime(timezone=True))
    clid = db.Column(db.String(80))
    src = db.Column(db.String(80))
    dst = db.Column(db.String(80))
    dcontext = db.Column(db.String(80))
    channel = db.Column(db.String(80))
    dstchannel = db.Column(db.String(80))
    lastapp = db.Column(db.String(80))
    lastdata = db.Column(db.String(80))
    duration = db.Column(db.Integer)
    disposition = db.Column(db.String(45))
    uniqueid = db.Column(db.String(32))
    filename = db.Column(db.String(32))

    def __repr__(self):
        return '<Cdr {}>'.format(self.uniqueid)


class Chop(db.Model):
    __tablename__ = 'chop'
    __searchable__ = ['_title', '_comment', '_phone']

    id = db.Column(db.Integer, primary_key=True)
    _title = db.Column('title', db.String(255))
    _comment = db.Column('comment', db.String(255))
    _phone = db.Column('phone', db.String(255))
    rotation_time = db.Column(db.Time(timezone=True))

    users = db.relationship("User", secondary=chop_users, back_populates="chops")
    mobile_groups = db.relationship("MobileGroup", back_populates="chop")

    reacted_objects = db.relationship("Object", foreign_keys='Object.reaction_chop_id', backref='reacting_chop', lazy='dynamic')
    monitored_objects = db.relationship("Object", foreign_keys='Object.monitoringcenter_id', backref='monitoring_chop', lazy='dynamic')

    @property
    def title(self):
        return self._title.strip()

    @title.setter
    def title(self, value):
        self._title = value

    @property
    def comment(self):
        return self._comment.strip()

    @comment.setter
    def comment(self, value):
        self._comment = value

    @property
    def phone(self):
        return self._phone.strip()

    @phone.setter
    def phone(self, value):
        self._phone = value

    def __repr__(self):
        return '<Chop {}>'.format(self.title)


class Event(db.Model):
    __tablename__ = 'events'

    code = db.Column(db.String(3), primary_key=True)
    _open_ru = db.Column('open_ru', db.String(255))
    _close_ru = db.Column('close_ru', db.String(255))
    event_type_id = db.Column(db.Integer, db.ForeignKey('eventtype.id'))
    event_type = db.relationship("EventType", back_populates="events")

    @property
    def open_ru(self):
        return self._open_ru.strip()

    @open_ru.setter
    def open_ru(self, value):
        self._open_ru = value

    @property
    def close_ru(self):
        if self._close_ru:
            return self._close_ru.strip()
        else:
            return 'ВОССТ. {}'.format(self.open_ru)

    @close_ru.setter
    def close_ru(self, value):
        self._close_ru = value

    def __repr__(self):
        return '<Event {}, {}>'.format(self.code, self.open_ru)


class EventType(db.Model):
    __tablename__ = 'eventtype'

    id = db.Column(db.Integer, primary_key=True)
    priority = db.Column(db.Integer, nullable=False)
    is_incident = db.Column('generate_incident', db.Integer)
    _type_ru = db.Column('type_ru', db.String(255))
    _trouble_ru = db.Column('trouble_ru', db.String(255))

    events = db.relationship("Event", back_populates="event_type")
    messages = db.relationship("Message", backref="event_type", lazy='dynamic')
    troubles = db.relationship("Trouble", backref="event_type")

    @property
    def to_json(self):
        return json.dumps(self.__dict__, default=to_serializable)

    @property
    def to_dict(self):
        target = json.loads(self.to_json)
        target.update({'title': self.type_ru})
        target.update({'color': self.color})
        target.update({'fill_color': self.fill_color})
        target.update({'text_color': self.text_color})
        return target

    @property
    def type_ru(self):
        return self._type_ru.strip()

    @type_ru.setter
    def type_ru(self, value):
        self._type_ru = value

    @property
    def trouble_ru(self):
        return self._trouble_ru.strip()

    @trouble_ru.setter
    def trouble_ru(self, value):
        self._trouble_ru = value

    # @property
    # def color(self):
    #     if self.priority >= 20:
    #         return 'danger'
    #     elif 15 <= self.priority < 20:
    #         return 'warning'
    #     elif 10 <= self.priority < 15:
    #         return 'info'
    #     else:
    #         return 'default'

    @property
    def color(self):
        return event_types_map.get(self.id, 'bg_None')

    @property
    def fill_color(self):
        return event_colors.get(self.id, 'bg-none')

    @property
    def text_color(self):
        return event_text_colors.get(self.id, 'text-none')

    @cache.memoize(3600)
    def __iter__(self):
        all_events = self.query.all()
        return (x for x in all_events)

    def __repr__(self):
        return '<EventType {}, {}>'.format(self.id, self.trouble_ru)


class Device(db.Model):
    __tablename__ = 'device'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255))

    objects = db.relationship('Object', backref='device', lazy='dynamic')

    def __repr__(self):
        return '<Device {}>'.format(self.title)


class SubDevice(db.Model):
    __tablename__ = 'subdevice'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255))

    objects = db.relationship('Object', backref='subdevice', lazy='dynamic')

    def __repr__(self):
        return '<SubDevice {}>'.format(self.title)


class MobileGroup(db.Model):
    __tablename__ = 'mobile_groups'
    __searchable__ = ['title', 'comment', 'phone_mobile']

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255))
    comment = db.Column(db.String(255))
    phone_mobile = db.Column(db.String(15))
    status = db.Column(db.Integer)
    type = db.Column(db.Integer)

    chop_id = db.Column(db.Integer, db.ForeignKey('chop.id'))
    chop = db.relationship("Chop", back_populates="mobile_groups")

    objects = db.relationship("Object", secondary=object_gbrs, back_populates="mobile_groups")

    wialon_id = db.Column(db.Integer, db.ForeignKey('wialon_units.id'))
    wialon_unit = db.relationship("WialonUnit", back_populates="mobile_group")

    troubles = db.relationship("Trouble", secondary=trouble_gbrs, backref='mobile_groups', lazy='dynamic')

    def route(self, trouble_id):
        if self.wialon_unit is not None:
            points = []
            trouble = self.troubles.filter(Trouble.id == trouble_id).first()
            if trouble.closed_at is not None:
                messages = self.wialon_unit.messages.filter(WialonMessage.geom.isnot(None), WialonMessage.ts > trouble.opened_at, WialonMessage.ts < trouble.closed_at).all()
            else:
                messages = self.wialon_unit.messages.filter(WialonMessage.geom.isnot(None), WialonMessage.ts > trouble.opened_at).all()
            for m in messages:
                points.append(m.to_shape)
            return LineString(points).to_wkt() if len(points) > 1 else None
        else:
            return None

    @property
    # @cache.memoize(60)
    def reacted_troubles(self):
        now = datetime.utcnow()
        current_date = datetime.date(now)
        current_rotation_ts = now.replace(tzinfo=pytz.UTC)
        chop_rotation_ts = datetime.combine(current_date, self.chop.rotation_time)
        if current_rotation_ts > chop_rotation_ts:
            rotation_ts = datetime.combine(current_date, self.chop.rotation_time)
        else:
            yesterday = current_date - timedelta(1)
            rotation_ts = datetime.combine(yesterday, self.chop.rotation_time)
        reacted_troubles = self.troubles.filter(Trouble.opened_at > rotation_ts).all()
        return reacted_troubles

    @property
    def to_json(self):
        return json.dumps(self.__dict__, default=to_serializable)

    @property
    def to_dict(self):
        target = json.loads(self.to_json)
        target.update({'wialon_unit': self.wialon_unit.to_dict})
        return target

    def __repr__(self):
        return '<MobileGroup {}>'.format(self.title)


class Boundary(db.Model):
    __tablename__ = 'geo_boundary'
    __searchable__ = ['name']

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(63))
    osm_id = db.Column(NUMERIC)
    admin_lvl = db.Column(db.String(1))
    geom = db.Column(Geometry(geometry_type='MULTIPOLYGON', srid=4326))

    def __repr__(self):
        return '<GeoBoundary {}>'.format(self.name)


class Message(db.Model):
    __tablename__ = 'messages'

    id = db.Column(db.Integer, primary_key=True)
    event_code = db.Column(db.String(3), nullable=False)
    _qualifier = db.Column('qualifier', db.Integer, nullable=False)
    partition_code = db.Column(db.String(2), nullable=False)
    zone_code = db.Column(db.String(3), nullable=False)

    event_text = db.Column(db.String(3000))
    user_comment = db.Column(db.String(3000))

    timestamp = db.Column(db.DateTime(timezone=True))

    object_id = db.Column(db.Integer, db.ForeignKey('objects.id'))
    event_type_id = db.Column(db.Integer, db.ForeignKey('eventtype.id'))
    trouble_id = db.Column(db.Integer, db.ForeignKey('troubles.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('ansecuser.id'))
    channel_id = db.Column(db.Integer, db.ForeignKey('ansecline.id_'))

    @property
    def human_date(self):
        locale.setlocale(locale.LC_TIME, "ru_RU.UTF-8")
        return datetime.strftime(self.timestamp, '%b %d %H:%M:%S')

    @property
    def human_date_comma(self):
        locale.setlocale(locale.LC_TIME, "ru_RU.UTF-8")
        return datetime.strftime(self.timestamp, '%b %d, %H:%M:%S')
        # return datetime.strftime(self.timestamp, '%H:%M:%S')

    @property
    def panel_code(self):
        return '{:04d}'.format(self.object_id if self.object_id is not None else 0)

    @property
    def qualifier(self):
        return 'E' if self._qualifier == 1 else 'R'

    @qualifier.setter
    def qualifier(self, value):
        self._qualifier = 1 if value in ['1', 'E'] else 3

    @property
    def bg_color(self):
        return self.event_type.color

    @property
    def contact_id(self):
        # return '{o:04d}{q}{e:03d}{p:02d}{z:03d}'.format(o=self.object_id, q=self.qualifier, e=self.event_code, p=self.partition_code, z=self.zone_code)
        return '{q}{e}{p}{z}'.format(q=self.qualifier, e=self.event_code, p=self.partition_code, z=self.zone_code)

    def __repr__(self):
        return "<Message {}, {}>".format(self.id, self.contact_id)


class Object(db.Model):
    __tablename__ = 'objects'
    __searchable__ = ['object_id_str', 'title', 'address']

    id = db.Column(db.Integer, primary_key=True)
    object_id_str = db.Column(db.String(10))
    comm_account = db.Column(db.Integer)
    comm_section = db.Column(db.Integer)

    title = db.Column(db.String(255))
    # _title = db.Column('title', db.String(255))
    _comment = db.Column('comment', db.String(255))

    address = db.Column('address', db.String(255))

    started_at = db.Column(db.Date)
    lastevent_at = db.Column(db.DateTime(timezone=True))

    status = db.Column(db.Integer)

    _hotline = db.Column('hotline', db.Integer)
    _service_direct = db.Column('service_direct', db.Integer)
    _service_falck = db.Column('service_falck', db.Integer)
    _security_post = db.Column('security_post', db.Integer)
    security_post_name = db.Column(db.String(255))
    security_post_phone = db.Column(db.String(255))
    _police_phone = db.Column('police_phone', db.String(255))
    geom = db.Column(Geometry(geometry_type='POINT', srid=4326))

    device_id = db.Column(db.Integer, db.ForeignKey('device.id'))
    subdevice_id = db.Column(db.Integer, db.ForeignKey('subdevice.id'))
    family_id = db.Column(db.Integer, db.ForeignKey('objecttype.id'))
    category_id = db.Column(db.Integer, db.ForeignKey('objectcategory.id'))
    equipment_id = db.Column(db.Integer, db.ForeignKey('tsotype.id'))

    region_id = db.Column(db.Integer, db.ForeignKey('region_part.id'))

    monitoringcenter_id = db.Column(db.Integer, db.ForeignKey('chop.id'))
    reaction_chop_id = db.Column(db.Integer, db.ForeignKey('chop.id'))

    importance_id = db.Column(db.Integer, db.ForeignKey('objectimportance.id'))
    territory_id = db.Column(db.Integer, db.ForeignKey('objectterritory.id'))
    service_id = db.Column(db.Integer, db.ForeignKey('servicetype.id'))
    reaction_id = db.Column(db.Integer, db.ForeignKey('reacttype.id'))
    wialon_unit_id = db.Column(db.Integer, db.ForeignKey('wialon_units.id'))

    # importance = db.relationship("Importance", back_populates="objects")
    # category = db.relationship("Category", back_populates="objects")
    wialon_unit = db.relationship("WialonUnit", back_populates="object", uselist=False)

    persons = db.relationship('Person', secondary=object_persons, back_populates='objects')
    mobile_groups = db.relationship('MobileGroup', secondary=object_gbrs, back_populates='objects')

    photos = db.relationship("Photo")
    schemes = db.relationship("Scheme")
    partitions = db.relationship("Partition", backref='object', lazy='dynamic')
    zones = db.relationship("Zone", backref='object', lazy='dynamic')

    messages = db.relationship("Message", backref="object", lazy='dynamic')
    troubles = db.relationship("Trouble", backref="object", lazy='dynamic')

    # @property
    # def to_dict(self):
    #     results = self.__dict__
    #     persons_list = list()
    #     for p in self.persons:
    #         persons_list.append(p.__dict__)
    #     results.update({'persons': persons_list})
    #     return results

    @property
    def to_json(self):
        return json.dumps(self.__dict__, default=to_serializable)

    @property
    def to_dict(self):
        target = json.loads(self.to_json)
        target.update({'status_icon': self.status_icon})
        target.update({'title_shortened': self.title_shortened})
        target.update({'panel_code': self.panel_code})
        target.update({'lon': self.location.x if self.location is not None else None})
        target.update({'lat': self.location.y if self.location is not None else None})
        return target

    @property
    def police_phone(self):
        return self._police_phone if self._police_phone != '' and self._police_phone is not None else ''

    @property
    def hotline(self):
        return '<i class="fa fa-phone"></i>&nbsp;Hotline' if self._hotline > 0 else ''

    @property
    def security_post(self):
        return '<i class="fa fa-shield"></i>&nbsp;{}'.format(self.security_post_name) if self._security_post > 0 else ''

    @property
    def service_direct(self):
        return '<i class="fa fa-cog"></i>&nbsp;Direct' if self._service_direct > 0 else ''

    @property
    def service_falck(self):
        return '<i class="fa fa-cog"></i>&nbsp;Falck' if self._service_falck > 0 else ''

    @property
    def title_shortened(self):
        return shorten(self.title, width=32, placeholder="..")

    # @property
    # def title(self):
    #     return self._title.strip()
    #
    # @title.setter
    # def title(self, value):
    #     self._title = value

    @property
    def is_armed(self):
        for part in self.partitions:
            print(part.state.get('armed', False))
        return True

    @property
    def comment(self):
        return self._comment.strip() if self._comment else None

    @comment.setter
    def comment(self, value):
        self._comment = value

    @property
    def status_css(self):
        if self.status == 3:
            return 'success'
        elif self.status == 2:
            return 'warning'
        elif self.status == 1:
            return 'danger'
        else:
            return 'default'

    @property
    def status_text(self):
        if self.status == 3:
            return '<i class="fa fa-check"></i>&nbsp;Активный'
        elif self.status == 2:
            return '<i class="fa fa-wrench"></i>&nbsp;В сервисе'
        elif self.status == 1:
            return '<i class="fa fa-ban"></i>&nbsp;Остановлен'
        else:
            return '<i class="fa fa-times"></i>&nbsp;Отключен'

    @property
    def status_icon(self):
        if self.status == 3:
            return '<i class="fa fa-check fa-fw text-success"></i>'
        elif self.status == 2:
            return '<i class="fa fa-wrench fa-fw text-warning"></i>'
        elif self.status == 1:
            return '<i class="fa fa-ban fa-fw text-danger"></i>'
        else:
            return '<i class="fa fa-times fa-fw"></i>'

    @property
    def zones_ordered(self):
        return self.zones.order_by(Zone.code.asc()).all()

    @property
    def partitions_ordered(self):
        return self.partitions.order_by(Partition.code.asc()).all()

    @property
    def partition_states(self):
        states = dict()
        print(self.partitions_ordered)
        for part in self.partitions.all():
            states.update({part.code: part.state['armed']})
        print(states)
        return states

    @property
    # @cache.memoize(60)
    def recent_troubles(self):
        now = datetime.utcnow()
        current_date = datetime.date(now)
        current_rotation_ts = now.replace(tzinfo=pytz.UTC)
        chop_rotation_ts = datetime.combine(current_date, self.monitoring_chop.rotation_time)
        if current_rotation_ts > chop_rotation_ts:
            rotation_ts = datetime.combine(current_date, self.monitoring_chop.rotation_time)
        else:
            yesterday = current_date - timedelta(1)
            rotation_ts = datetime.combine(yesterday, self.monitoring_chop.rotation_time)
        return self.troubles.filter(Trouble.opened_at > rotation_ts).order_by(Trouble.id.desc()).all()

    @property
    # @cache.memoize(60)
    def recent_messages(self):
        now = datetime.utcnow()
        current_date = datetime.date(now)
        current_rotation_ts = now.replace(tzinfo=pytz.UTC)
        chop_rotation_ts = datetime.combine(current_date, self.monitoring_chop.rotation_time)
        if current_rotation_ts > chop_rotation_ts:
            rotation_ts = datetime.combine(current_date, self.monitoring_chop.rotation_time)
        else:
            yesterday = current_date - timedelta(1)
            rotation_ts = datetime.combine(yesterday, self.monitoring_chop.rotation_time)
        return self.messages.filter(Message.timestamp > rotation_ts).order_by(Message.id.desc()).all()

    @property
    def panel_code(self):
        return '{:04d}'.format(self.comm_account if self.comm_account is not None else self.id)

    @property
    def location(self):
        return shape.to_shape(self.geom) if self.geom is not None else None
        # return json.dumps({'lat': point.x, 'lon': point.y})
        # return wkt.dumps(shape.to_shape(self.geom))
        # return self.geom.wkt

    @property
    def view_url(self):
        return '/objects/{}'.format(self.id)

    def alarm(self):
        if self.work_status == WorkStatus.DISABLED:
            return False
        try:
            url = 'http://{}/_pool/doevent/?name=wialon&code={}&e=E{}00000'.format(Config.OPENCSM_HOST, self.code, Config.OPENCSM_CID)
            headers = {'authorization': Config.OPENCSM_KEY}
            r = requests.get(url, headers=headers)
            if r.status_code == 200:
                return True
        except Exception as e:
            print("Unable to get URL. {}".format(e))
            return False

    def __repr__(self):
        return '<Object {}, {}>'.format(self.id, self.title)


class Category(db.Model):
    __tablename__ = 'objectcategory'
    __searchable__ = ['_title', '_comment']

    id = db.Column(db.Integer, primary_key=True)
    _title = db.Column('title', db.String(255))
    _comment = db.Column('comment', db.String(255))

    objects = db.relationship('Object', backref='category', lazy='dynamic')

    @property
    def title(self):
        return self._title.strip()

    @title.setter
    def title(self, value):
        self._title = value

    @property
    def comment(self):
        return self._comment.strip()

    @comment.setter
    def comment(self, value):
        self._comment = value

    def __repr__(self):
        return '<Category {}>'.format(self.title)


class Importance(db.Model):
    __tablename__ = 'objectimportance'
    id = db.Column(db.Integer, primary_key=True)
    priority = db.Column(db.Integer, nullable=False)
    _title = db.Column('title', db.String(255))
    _comment = db.Column('comment', db.String(255))

    objects = db.relationship('Object', backref="importance", lazy='dynamic')

    @property
    def title(self):
        return self._title.strip()

    @title.setter
    def title(self, value):
        self._title = value

    @property
    def comment(self):
        return self._comment.strip()

    @comment.setter
    def comment(self, value):
        self._comment = value

    def __repr__(self):
        return '<Importance {}>'.format(self.title)


class Territory(db.Model):
    __tablename__ = 'objectterritory'
    id = db.Column(db.Integer, primary_key=True)
    _title = db.Column('title', db.String(255))
    _comment = db.Column('comment', db.String(255))

    objects = db.relationship('Object', backref='territory', lazy='dynamic')

    @property
    def title(self):
        return self._title.strip()

    @title.setter
    def title(self, value):
        self._title = value

    @property
    def comment(self):
        return self._comment.strip()

    @comment.setter
    def comment(self, value):
        self._comment = value

    def __repr__(self):
        return '<Territory {}>'.format(self.title)


class Family(db.Model):
    __tablename__ = 'objecttype'
    id = db.Column(db.Integer, primary_key=True)
    _title = db.Column('title', db.String(255))
    _comment = db.Column('comment', db.String(255))

    objects = db.relationship('Object', backref='family', lazy='dynamic')

    @property
    def title(self):
        return self._title.strip()

    @title.setter
    def title(self, value):
        self._title = value

    @property
    def comment(self):
        return self._comment.strip()

    @comment.setter
    def comment(self, value):
        self._comment = value

    def __repr__(self):
        return '<Family {}>'.format(self.title)


class Partition(db.Model):
    __tablename__ = 'partitions'
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.Integer)
    title = db.Column(db.String(255))
    state = db.Column(JSON)
    schedule = db.Column(JSONB)

    object_id = db.Column(db.Integer, db.ForeignKey('objects.id'))

    @property
    def code_phonetic(self):
        return '{:02d}'.format(self.code)

    zones = db.relationship('Zone')

    def __repr__(self):
        return '<Partition {}, {}>'.format(self.code, self.title)


class Person(db.Model):
    __tablename__ = 'persons'
    __searchable__ = ['name', 'surname', 'email', 'keyword', 'phone_mobile']

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    surname = db.Column(db.String(255))
    middle_name = db.Column(db.String(255))
    email = db.Column(db.String(255))
    status = db.Column(db.String(255))
    keyword = db.Column(db.String(255))
    comment = db.Column(db.String(255))
    phone_mobile = db.Column(db.String(11))
    phone_work = db.Column(db.String(255))
    phone_home = db.Column(db.String(255))

    objects = db.relationship("Object", secondary=object_persons, back_populates="persons")

    @property
    def popover(self):
        return '<div><i class="fa fa-key fa-fw"></i>&nbsp;{}</div>' \
               '<div><i class="fa fa-phone fa-fw"></i>&nbsp;w: {}</div>' \
               '<div><i class="fa fa-phone fa-fw"></i>&nbsp;h: {}</div>' \
               '<div><i class="fa fa-comment fa-fw"></i>&nbsp;{}</div>'.format(self.keyword, self.phone_work, self.phone_home, self.comment)

    @property
    def to_json(self):
        return json.dumps(self.__dict__, default=to_serializable)

    @property
    def full_name(self):
        return '{} {}'.format(self.name, self.surname)

    @property
    def url_encoded_name(self):
        return parse.quote(self.name) if self.name is not None else ''

    @property
    def url_encoded_surname(self):
        return parse.quote(self.surname) if self.surname is not None else ''

    @property
    def url_encoded_fullname(self):
        return parse.quote(self.full_name.replace(' ', '_')) if self.full_name is not None else ''

    @property
    def priority(self):
        if len(self.objects) == 0:
            return 0, ''
        elif self.objects[0].importance is None:
            return 0, ''
        else:
            return self.objects[0].importance.priority, self.objects[0].importance.title

    # @property
    # def phone_m(self):
    #     try:
    #         x = phonenumbers.parse(self._phone_m, 'RU')
    #         if len(str(x.national_number)) == 10:
    #             return '{}{}'.format(x.country_code, x.national_number)
    #         elif len(str(x.national_number)) == 7:
    #             return '{}812{}'.format(x.country_code, x.national_number)
    #     except NumberParseException:
    #         pass
    #
    # @phone_m.setter
    # def phone_m(self, value):
    #     x = phonenumbers.parse(value, 'RU')
    #     self._phone_m = '{}{}'.format(x.country_code, x.national_number)

    def __repr__(self):
        return '<Person {}>'.format(self.surname)


class Photo(db.Model):
    __tablename__ = 'foto'

    id = db.Column(db.Integer, primary_key=True)
    file_num = db.Column(db.Integer)
    _title = db.Column('title', db.String(255))
    _comment = db.Column('comment', db.String(255))

    object_id = db.Column(db.Integer, db.ForeignKey('objects.id'))
    object = db.relationship("Object", back_populates="photos")

    @property
    def url(self):
        return '/external_images/foto/{id:010d}/foto_{num}.jpg'.format(id=self.object_id, num=self.file_num)

    @property
    def title(self):
        return self._title.strip()

    @title.setter
    def title(self, value):
        self._title = value

    @property
    def comment(self):
        return self._comment.strip()

    @comment.setter
    def comment(self, value):
        self._comment = value

    def __repr__(self):
        return '<Photo {}>'.format(self.id)


class Reaction(db.Model):
    __tablename__ = 'reacttype'
    id = db.Column(db.Integer, primary_key=True)
    _title = db.Column('title', db.String(255))
    _comment = db.Column('comment', db.String(255))

    objects = db.relationship('Object', backref='reaction', lazy='dynamic')

    @property
    def title(self):
        return self._title.strip()

    @title.setter
    def title(self, value):
        self._title = value

    @property
    def comment(self):
        return self._comment.strip()

    @comment.setter
    def comment(self, value):
        self._comment = value

    def __repr__(self):
        return '<Reaction {}>'.format(self.title)


class Region(db.Model):
    __tablename__ = 'region_part'
    id = db.Column(db.Integer, primary_key=True)
    _title = db.Column('title', db.String(255))
    _comment = db.Column('comment', db.String(255))

    objects = db.relationship('Object', backref='region', lazy='dynamic')

    @property
    def title(self):
        return self._title.strip()

    @title.setter
    def title(self, value):
        self._title = value

    @property
    def comment(self):
        return self._comment.strip()

    @comment.setter
    def comment(self, value):
        self._comment = value

    def __repr__(self):
        return '<Region {}>'.format(self.title)


class Service(db.Model):
    __tablename__ = 'servicetype'
    id = db.Column(db.Integer, primary_key=True)
    _title = db.Column('title', db.String(255))
    _comment = db.Column('comment', db.String(255))

    objects = db.relationship('Object', backref='service', lazy='dynamic')

    @property
    def title(self):
        return self._title.strip()

    @title.setter
    def title(self, value):
        self._title = value

    @property
    def comment(self):
        return self._comment.strip()

    @comment.setter
    def comment(self, value):
        self._comment = value

    def __repr__(self):
        return '<Service {}>'.format(self.title)


class Sms(db.Model):
    __tablename__ = 'sms'
    id = db.Column(db.Integer, primary_key=True)

    def __repr__(self):
        return '<Sms %r>'.format(self.id)


class Scenario(db.Model):
    __tablename__ = 'scenario'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255))

    def __repr__(self):
        return '<Scenario {}>'.format(self.title)


class Scheme(db.Model):
    __tablename__ = 'schema'
    id = db.Column(db.Integer, primary_key=True)
    file_num = db.Column(db.Integer)
    _title = db.Column('title', db.String(255))
    _comment = db.Column('comment', db.String(255))

    object_id = db.Column(db.Integer, db.ForeignKey('objects.id'))
    object = db.relationship("Object", back_populates="schemes")

    @property
    def url(self):
        return '/external_images/schema/{id:010d}/schema_{num}.jpg'.format(id=self.object_id, num=self.file_num)

    @property
    def title(self):
        return self._title.strip()

    @title.setter
    def title(self, value):
        self._title = value

    @property
    def comment(self):
        return self._comment.strip()

    @comment.setter
    def comment(self, value):
        self._comment = value

    def __repr__(self):
        return '<Scheme {}>'.format(self.id)


class Trouble(db.Model):
    __tablename__ = 'troubles'
    id = db.Column(db.Integer, primary_key=True)
    opened = db.Column(db.Integer)
    status = db.Column(db.Integer)

    opened_at = db.Column(db.DateTime(timezone=True))
    closed_at = db.Column(db.DateTime(timezone=True))
    start_work_at = db.Column(db.DateTime(timezone=True))

    object_id = db.Column(db.Integer, db.ForeignKey('objects.id'))
    event_type_id = db.Column(db.Integer, db.ForeignKey('eventtype.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('ansecuser.id'))

    messages = db.relationship("Message", backref="trouble", lazy='dynamic')

    @property
    def human_date(self):
        locale.setlocale(locale.LC_TIME, "ru_RU.UTF-8")
        return datetime.strftime(self.opened_at, '%c')

    @property
    def to_json(self):
        return json.dumps(self.__dict__, default=to_serializable)

    @property
    def to_dict(self):
        return json.loads(self.to_json)

    @property
    def routes(self):
        results = []
        print(self.mobile_groups)
        for m in self.mobile_groups:
            print('route={}'.format(m.route(self.id)))
            results.append(m.route(self.id))
        print(results)
        return results

    @property
    def to_dict_full(self):
        full_dict = json.loads(self.to_json)
        full_dict.update({'object': self.object.to_dict})
        full_dict.update({'event_type': self.event_type.to_dict})
        full_dict.update({'opened_at': self.opened_at.isoformat()})
        full_dict.update({'closed_at': self.closed_at.isoformat() if self.closed_at else None})
        full_dict.update({'start_work_at': self.start_work_at.isoformat() if self.start_work_at else None})
        return full_dict

    def __repr__(self):
        return '<Trouble {}>'.format(self.id)


class Equipment(db.Model):
    __tablename__ = 'tsotype'
    id = db.Column(db.Integer, primary_key=True)
    _title = db.Column('title', db.String(255))
    _comment = db.Column('comment', db.String(255))

    objects = db.relationship('Object', backref='equipment', lazy='dynamic')

    @property
    def title(self):
        return self._title.strip()

    @title.setter
    def title(self, value):
        self._title = value

    @property
    def comment(self):
        return self._comment.strip()

    @comment.setter
    def comment(self, value):
        self._comment = value

    def __repr__(self):
        return '<Equipment {}>'.format(self.title)


class User(db.Model):
    __tablename__ = 'ansecuser'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column('login', db.String(64), index=True, unique=True)
    password = db.Column('password', db.String(120))
    email = db.Column('email', db.String(120), index=True, unique=True)
    realname = db.Column(db.String(256))
    last_seen = db.Column(db.DateTime(timezone=True))
    acl_id = db.Column(db.Integer, db.ForeignKey('acl.id'))

    acl = db.relationship("Acl", back_populates="users")
    messages = db.relationship("Message", backref="user")
    troubles = db.relationship("Trouble", backref="user")

    chops = db.relationship("Chop", secondary=chop_users, back_populates="users")

    def __init__(self, username, password):
        self.username = username
        self.password = password

    @property
    @cache.memoize(60)
    def total_objects(self):
        total_objects = 0
        for chop in self.chops:
            total_objects += chop.monitored_objects.count()
        return total_objects

    @property
    @cache.memoize(60)
    def troubles(self):
        total_troubles = list()
        for chop in self.chops:
            chop_troubles = Trouble.query.filter(Trouble.opened == 1, Trouble.object_id.in_(chop.monitored_objects.with_entities(Object.id))).all()
            total_troubles.extend(chop_troubles)
        return total_troubles

    @property
    @cache.memoize(60)
    def troubles_list(self):
        total_troubles = list()
        for chop in self.chops:
            now = datetime.utcnow()
            current_date = datetime.date(now)
            current_rotation_ts = now.replace(tzinfo=pytz.UTC)
            chop_rotation_ts = datetime.combine(current_date, chop.rotation_time)
            if current_rotation_ts > chop_rotation_ts:
                rotation_ts = datetime.combine(current_date, chop.rotation_time)
            else:
                yesterday = current_date - timedelta(1)
                rotation_ts = datetime.combine(yesterday, chop.rotation_time)
            chop_troubles = Trouble.query.filter(Trouble.opened_at > rotation_ts, Trouble.object_id.in_(chop.monitored_objects.with_entities(Object.id))).all()
            total_troubles.extend(chop_troubles)
        return total_troubles

    @property
    @cache.memoize(60)
    def recent_troubles_cached(self):
        recent_troubles = list()
        grouped_troubles = dict()
        for chop in self.chops:
            now = datetime.utcnow()
            current_date = datetime.date(now)
            current_rotation_ts = now.replace(tzinfo=pytz.UTC)
            chop_rotation_ts = datetime.combine(current_date, chop.rotation_time)
            if current_rotation_ts > chop_rotation_ts:
                rotation_ts = datetime.combine(current_date, chop.rotation_time)
            else:
                yesterday = current_date - timedelta(1)
                rotation_ts = datetime.combine(yesterday, chop.rotation_time)
            chop_troubles = Trouble.query.filter(Trouble.opened_at > rotation_ts, Trouble.object_id.in_(chop.monitored_objects.with_entities(Object.id))).all()
            recent_troubles.extend(chop_troubles)
        for trouble in recent_troubles:
            if trouble.event_type not in grouped_troubles:
                grouped_troubles[trouble.event_type] = list()
            grouped_troubles[trouble.event_type].append(trouble)
        return collections.OrderedDict(sorted(grouped_troubles.items(), key=lambda k: k[0].priority, reverse=True))

    @property
    # @cache.memoize(60)
    def recent_troubles(self):
        recent_troubles = list()
        grouped_troubles = dict()
        for chop in self.chops:
            now = datetime.utcnow()
            current_date = datetime.date(now)
            current_rotation_ts = now.replace(tzinfo=pytz.UTC)
            chop_rotation_ts = datetime.combine(current_date, chop.rotation_time)
            if current_rotation_ts > chop_rotation_ts:
                rotation_ts = datetime.combine(current_date, chop.rotation_time)
            else:
                yesterday = current_date - timedelta(1)
                rotation_ts = datetime.combine(yesterday, chop.rotation_time)
            chop_troubles = Trouble.query.filter(Trouble.opened_at > rotation_ts, Trouble.object_id.in_(chop.monitored_objects.with_entities(Object.id))).all()
            recent_troubles.extend(chop_troubles)
        for trouble in recent_troubles:
            if trouble.event_type not in grouped_troubles:
                grouped_troubles[trouble.event_type] = list()
            grouped_troubles[trouble.event_type].append(trouble)
        return collections.OrderedDict(sorted(grouped_troubles.items(), key=lambda k: k[0].priority, reverse=True))

    # @property
    # @cache.memoize(60)
    # def grouped_troubles(self):
    #     grouped_troubles = dict()
    #     for trouble in self.recent_troubles:
    #         if trouble.event_type not in grouped_troubles:
    #             grouped_troubles[trouble.event_type] = list()
    #         else:
    #             grouped_troubles[trouble.event_type].append(trouble)
    #     return collections.OrderedDict(sorted(grouped_troubles.items(), key=lambda k: k[0].priority, reverse=True))

    # @property
    # @cache.memoize(60)
    # def recent_troubled_objects(self):
    #     recent_troubled_objects = dict()
    #     for event_type in self.grouped_troubles:
    #
    #     return None

    def has_perm(self, perm):
        return perm in self.perms

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    @property
    def perms(self):
        return self.acl.perms if self.acl is not None else []

    def get_id(self):
        return str(self.id)  # python 3

    def get_email(self):
        return str(self.email)  # python 3

    def avatar(self, size):
        return 'https://www.gravatar.com/avatar/{}?d=identicon&r=PG&s={}'.format(md5(self.email.encode()).hexdigest(), str(size))

    def __repr__(self):
        return '<{} {}>'.format(self.__class__.__name__, self.username)


class WialonMessage(db.Model):
    __tablename__ = 'wialon_msgs'
    id = db.Column(db.Integer, primary_key=True)
    ts = db.Column(db.DateTime(timezone=True))
    speed = db.Column(db.Integer)
    course = db.Column(db.Integer)
    z = db.Column(db.Integer)
    sats = db.Column(db.Integer)
    hdop = db.Column(db.Float)
    inputs = db.Column(db.Integer)
    outputs = db.Column(db.Integer)
    adc = db.Column(db.String(255))
    ibutton = db.Column(db.String(255))
    params = db.Column(JSONB)
    wialon_unit_id = db.Column(db.Integer, db.ForeignKey('wialon_units.id'))
    geom = db.Column(Geometry(geometry_type='POINT', srid=4326))

    @property
    def to_shape(self):
        return shape.to_shape(self.geom) if self.geom is not None else None

    def __repr__(self):
        return '<WialonMessage {}>'.format(self.id)


class WialonUnit(db.Model):
    __tablename__ = 'wialon_units'

    id = db.Column(db.Integer, primary_key=True)
    hw = db.Column(db.Integer)
    nm = db.Column(db.String(255))
    uid = db.Column(db.String(255))
    ph = db.Column(db.String(15))
    ph2 = db.Column(db.String(15))
    psw = db.Column(db.String(15))
    speed = db.Column(db.Integer)
    course = db.Column(db.Integer)
    satellites = db.Column(db.Integer)
    z = db.Column(db.Integer)
    last_msg = db.Column(db.DateTime(timezone=True))

    object = db.relationship("Object", uselist=False, back_populates="wialon_unit")
    mobile_group = db.relationship("MobileGroup", uselist=False, back_populates="wialon_unit")

    messages = db.relationship("WialonMessage", backref="wialon_unit", lazy='dynamic')

    geom = db.Column(Geometry(geometry_type='POINT', srid=4326))

    def trouble_messages(self, trouble_id):
        trouble = Trouble.query.get(trouble_id)
        if trouble.closed_at is not None:
            return self.messages.filter(WialonMessage.ts > trouble.opened_at, WialonMessage.ts < trouble.closed_at).all()
        else:
            return self.messages.filter(WialonMessage.ts > trouble.opened_at).all()

    def trouble_route(self, trouble_id):
        points = []
        messages = self.trouble_messages(trouble_id)
        for m in messages:
            points.append(m.to_shape)
        return LineString(points).to_wkt() if len(points) > 1 else None

    @property
    def to_wkt(self):
        return shape.to_shape(self.geom).to_wkt() if self.geom is not None else None

    @property
    def location(self):
        return shape.to_shape(self.geom) if self.geom is not None else None

    @property
    def to_json(self):
        return json.dumps(self.__dict__, default=to_serializable)

    @property
    def to_dict(self):
        target = json.loads(self.to_json)
        target.update({'geom_wkt': self.to_wkt})
        target.update({'last_msg': self.last_msg.isoformat()})
        target.update({'lon': self.location.x if self.location is not None else None})
        target.update({'lat': self.location.y if self.location is not None else None})
        return target

    # def trouble_messages(self):
    #     trouble = Trouble.query.get(trouble_id)
    #     return self.messages.filter(WialonMessage.ts > trouble.opened_at, WialonMessage.ts < trouble.closed_at).all()
    #     return shape.to_shape(self.geom) if self.geom is not None else None
    #     # return json.dumps({'lat': point.x, 'lon': point.y})
    #     # return wkt.dumps(shape.to_shape(self.geom))
    #     # return self.geom.wkt

    def __repr__(self):
        return '<WialonUnit {}>'.format(self.nm)


class Zone(db.Model):
    __tablename__ = 'zones'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255))
    sensor = db.Column(db.String(255))
    code = db.Column(db.Integer)
    isperson = db.Column(db.Boolean)
    state = db.Column(JSON)

    object_id = db.Column(db.Integer, db.ForeignKey('objects.id'))
    partition_id = db.Column(db.Integer, db.ForeignKey('partitions.id'))

    def __repr__(self):
        return '<Zone {}, {}>'.format(self.code, self.title)


flask_whooshalchemyplus.init_app(app)
